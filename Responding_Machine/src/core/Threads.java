package core;

import java.util.Date;

class Threads implements Runnable {
   public Thread t;
   public String id;
   public String [] resultList=null;
   public String query;
   public String callFuntion;
   
   Threads( String name, String inputQuery, String funtion) {
      id = name;
      query = inputQuery;
      callFuntion = funtion;
   }
   
   public void run() {
	 
	    resultList = BotController.method_justComment(query , id + "\t" + BotController.numberOfThread,  callFuntion);
    
   }
   
   public void start () {
   
      if (t == null) {
         t = new Thread (this, id);
         t.start ();
      }
   }
}
