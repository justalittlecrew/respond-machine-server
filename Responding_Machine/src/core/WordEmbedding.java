package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import mylibrary.MyLib_General;
import mylibrary.MyLib_NLP;

public class WordEmbedding {
	public static String sourceWordEmbeddingData = "glove.6B.200d.txt";
	
	static void buildPostCmntCosineSimilarityScore(){
		Map<String, String> postIdAndPostContent = new HashMap<>();
		Map<String, String> cmntIdAndCmntContent = new HashMap<>();		
		Map<String, String> PostCmntCosineSimilarityScore = new HashMap<>();		
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\post\\filteredPostWithWeighting"));

			String line;
			while ((line = reader.readLine()) != null) {
				postIdAndPostContent.put(MyLib_General.getFieldData(line, 1, "\t"), MyLib_General.getFieldData(line, 2, "\t") + "\t"+MyLib_General.getFieldData(line, 3, "\t"));
			}
			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println(postIdAndPostContent.size());
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\filteredCommentWithWeighting"));

			String line;
			while ((line = reader.readLine()) != null) {
				cmntIdAndCmntContent.put(MyLib_General.getFieldData(line, 1, "\t"), MyLib_General.getFieldData(line, 2, "\t")+ "\t"+MyLib_General.getFieldData(line, 3, "\t") );
			}
			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.println(cmntIdAndCmntContent.size());
		
		
		
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\indexFile"));
			String line1;
			int counter=0;
			while ((line1 = reader.readLine()) != null) {
			
				String cmntId = line1.split("\t")[1];
				String postId = line1.split("\t")[0];
				
				String postStringAndWeighting = postIdAndPostContent.get(postId);
				String cmntStringAndWeighting = cmntIdAndCmntContent.get(cmntId);
				String postString=MyLib_General.getFieldData(postStringAndWeighting, 1, "\t");
				String cmntString=MyLib_General.getFieldData(cmntStringAndWeighting, 1, "\t");

				
				counter+=1;
				if (counter%100000==0) 	System.out.println(counter); 
				if (postStringAndWeighting==null) continue;
				if (cmntStringAndWeighting==null) continue;
				//if (postString.trim().equals("")|cmntString.trim().equals("")) continue;
				
				
				postString= MyLib_General.deDup(postString);
				cmntString= MyLib_General.deDup(cmntString);				
				
				String weighting1 = MyLib_General.getFieldData(postStringAndWeighting, 2, "\t");

				String [] weightingList1 = weighting1.split(" ");
				double [] weightingarray1 = new double [weightingList1.length];
			
				for (int i =0;i<weightingList1.length;i++){
					if (weightingList1[i].trim().equals("")) continue;
					weightingarray1[i] = Double.parseDouble(weightingList1[i]); 
				}				

				
				String weighting2 = MyLib_General.getFieldData(cmntStringAndWeighting, 2, "\t").trim();
				
				
				String [] weightingList2 = weighting2.split(" ");
			
				double [] weightingarray2 = new double [weightingList2.length];
			
				for (int i =0;i<weightingList2.length;i++){
					if (weightingList2[i].trim().equals("")) continue;
					weightingarray2[i] = Double.parseDouble(weightingList2[i]); 
				}				
				
				

				double sim_score = MyLib_NLP.cosine_Similarity(postString,weightingarray1 ,cmntString,weightingarray2);

				if (weightingarray2.length>=3){
						sim_score *= Math.sqrt(3.0/weightingarray2.length);
				}

				PostCmntCosineSimilarityScore.put(postId + "\t"+ cmntId,sim_score +"");

			}
			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		
	
		try {
			PrintWriter writer = new PrintWriter("PostCmntCosineSimilarityScore");

			//================================= start display
			int counter4=0;
			for(Entry<String, String> entry : PostCmntCosineSimilarityScore.entrySet()) {
			    String key = entry.getKey();
			  String value = entry.getValue();
			  writer.println(key+ "\t"+ value);
			    counter4+=1;
			    if (counter4%10000==0) System.out.println(counter4);
			}
			//================================= end display
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	static void buildPostCmntSentencesEmbeddingScore(){
		Map<String, double[]> WordEmbedding = new HashMap<>();
		Map<String, String> postIdAndPostContent = new HashMap<>();
		Map<String, String> cmntIdAndCmntContent = new HashMap<>();		
		Map<String, String> cmntAndPostEmbeddingScore = new HashMap<>();				
				
		
		try {

			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\Embedding\\" + "cmnt_smaller_word_embedding"));
			
			String line;
			int counter = 0;
			  while ((line = reader.readLine()) != null) {
			  
		//	  System.out.println(line);
			  String wordKey = MyLib_General.getFieldData(line, 1, " ");
			  
			  String wordEmbeddingList [] =  line.split(" ");
			  double [] wordVector = new double [200];
			  
			  for (int i=0;i<200;i++){ 
				  wordVector[i] += Double.parseDouble(wordEmbeddingList[i+1]); 
			  }
			  WordEmbedding.put(wordKey,wordVector );
	  
			  }
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
			
			try {
				BufferedReader reader = new BufferedReader(
						new FileReader(System.getProperty("user.dir") + "\\post\\filtered_post"));

				String line;
				while ((line = reader.readLine()) != null) {
					postIdAndPostContent.put(MyLib_General.getFieldData(line, 1, "\t"), MyLib_General.getFieldData(line, 2, "\t"));
				}
				reader.close();

			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}
			System.out.println(postIdAndPostContent.size());
			
			try {
				BufferedReader reader = new BufferedReader(
						new FileReader(System.getProperty("user.dir") + "\\filtered_cmnt"));

				String line;
				while ((line = reader.readLine()) != null) {
					cmntIdAndCmntContent.put(MyLib_General.getFieldData(line, 1, "\t"), MyLib_General.getFieldData(line, 2, "\t"));
				}
				reader.close();

			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}
			System.out.println(cmntIdAndCmntContent.size());
			
			try {
				BufferedReader reader = new BufferedReader(
						new FileReader(System.getProperty("user.dir") + "\\indexFile"));
				String line1;
				int counter=0;
				while ((line1 = reader.readLine()) != null) {

					String cmntId = line1.split("\t")[1];
					String postId = line1.split("\t")[0];
					
					double [] v1 = new double[200];
					double [] v2 = new double[200];
					for (int i =0;i<v1.length;i++){
						v1[i]=v2[i]=0.0;
					}
					
					if (!(cmntIdAndCmntContent.containsKey(cmntId) && postIdAndPostContent.containsKey(postId))) continue;
							
					String [] cmntString = cmntIdAndCmntContent.get(cmntId).split(" ");
					String [] postString = postIdAndPostContent.get(postId).split(" ");
					
					
					for (int i=0;i<cmntString.length;i++){
						double [] wordEmbeddingVector = WordEmbedding.get(cmntString[i]);
						if (wordEmbeddingVector==null) continue;
						for (int j=0;j<wordEmbeddingVector.length;j++){
							v1[j]+=wordEmbeddingVector[j];
						}
					}
					for (int i=0;i<postString.length;i++){
						double [] wordEmbeddingVector = WordEmbedding.get(postString[i]);
						if (wordEmbeddingVector==null) continue;
						for (int j=0;j<wordEmbeddingVector.length;j++){
							v2[j]+=wordEmbeddingVector[j];
						}
					}					
					
					double semanticScore = calculateInnerProduct(v1, v2);
				
					if (cmntAndPostEmbeddingScore==null)
						cmntAndPostEmbeddingScore.put(postId + "\t" + cmntId,  "0");					
					else
						cmntAndPostEmbeddingScore.put(postId + "\t" + cmntId,  semanticScore + "");					
					
					counter++;
					//if (counter%10000==0)System.out.println(counter);
				}
				reader.close();

			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}

			
			try {
				PrintWriter writer = new PrintWriter("Embedding//" + "PostCmntSentencesEmbeddingScore");

				//================================= start display
				int counter4=0;
				for(Entry<String, String> entry : cmntAndPostEmbeddingScore.entrySet()) {
				    String key = entry.getKey();
				  String value = entry.getValue();
				  writer.println(key+ "\t"+ value);
				    counter4+=1;
				    
				}
				//================================= end display
				writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	static void createSentencesEmbedding(int flag) {
		
		long tStart = System.currentTimeMillis();
		
		Map<String, double[]> map = new HashMap<>();
		
		String sourceTermFrequency ="";
		String outputSmallerWordEmbeddingFile = "";
		String sourceFilterFile = "";
		String outputSentenceEmbeddingFile = "";
		
		if (flag==0){
			sourceTermFrequency = "\\post\\term_frequency";
		}
		else{
			sourceTermFrequency = "\\term_frequency";
		}
		if (flag==0){
			outputSmallerWordEmbeddingFile = "Embedding//" + "post_smaller_word_embedding";
		}
		else{
			outputSmallerWordEmbeddingFile ="Embedding//" + "cmnt_smaller_word_embedding";
		}
		if (flag==0){
			sourceFilterFile = "\\post\\filtered_post";
		}
		else{
			sourceFilterFile ="\\filtered_cmnt";
		}
		if (flag==0){
			outputSentenceEmbeddingFile = "post_sentence_embedding";
		}
		else{
			outputSentenceEmbeddingFile = "cmnt_sentence_embedding";
		}		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + sourceTermFrequency));

			String line;

			while ((line = reader.readLine()) != null) {

				map.put(line.split("\t")[0], new double[200]);

			}

			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		// System.out.println(Arrays.toString(map.get("apple")));
		// double[] temp = new double[200];
		// for (int i =0;i<200;i++) temp[i]=1.9999999;

		// map.put("apple", temp);
	

		int counter2 = 0;
		try {
			BufferedReader reader2 = new BufferedReader(
					new FileReader("C:\\Users\\Alan\\Desktop\\caps_support\\glove.6B\\" + sourceWordEmbeddingData));

			String line;

			while ((line = reader2.readLine()) != null) {
				String key = line.split(" ")[0];
				String[] vector = line.split(" ");
				// System.out.println("vector length= "+ vector.length + "key="
				// + key);

				if (map.containsKey(key)) {
					double[] aggregateVector = map.get(key);

					for (int i = 0; i < 200; i++) {
						// System.out.println(i);
						aggregateVector[i] += Double.parseDouble(vector[i + 1]);

					}

					map.put(key, aggregateVector);

				}

				counter2 += 1;
				// if (counter2 % 1000 == 0)
				// System.out.println(counter2);
			}

			reader2.close();
			// System.out.println(Arrays.toString(inputSentenceEmbedding));

			System.out.println("done2");

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		System.out.println("map size" + map.size());


		try {
			PrintWriter writer2 = new PrintWriter(outputSmallerWordEmbeddingFile);
			
			int counter4=0;
			for(Entry<String, double[]> entry : map.entrySet()) {
			    String key = entry.getKey();
			    double[] value = entry.getValue();

			  // System.out.println(Arrays.toString(value));
			    counter4+=1;
			    if (counter4 % 1000 == 0)
					System.out.println(counter4);
			    
			    String stringOfVectors = Arrays.toString(value).replace(",", "").replace("[", "").replace("]", "");
			    
			    writer2.println(key + " "+ stringOfVectors);
			 
			
			}
		//	System.out.print(counter4);
			writer2.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		
		int counter3 = 0;

		try {
			BufferedReader reader3 = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + sourceFilterFile));

			PrintWriter writer = new PrintWriter("Embedding//" + outputSentenceEmbeddingFile);

			String line;

			while ((line = reader3.readLine()) != null) {

				String[] sentenceList = MyLib_General.getFieldData(line, 2, "\t").split(" ");
				String id = line.split("\t")[0];

				double sentenceEmbedding[] = new double[200];

				// System.out.println(line);
				for (int i = 0; i < sentenceList.length; i++) {
					String key = sentenceList[i];   //each word in sentence

					if (map.containsKey(key)) {

						double mapVector[] = map.get(key);

						//System.out.println("key:"+ key+ " "+
					//	 Arrays.toString(mapVector));

						for (int j = 0; j < mapVector.length; j++) {
							sentenceEmbedding[j] += mapVector[j];
						}

					}
				}

				String outputSentenceEmbedding = "";
				for (int i = 0; i < sentenceEmbedding.length; i++)
					outputSentenceEmbedding += sentenceEmbedding[i] + " ";

				writer.println(id + "\t" + outputSentenceEmbedding);

				counter3 += 1;
				if (counter3 % 1000 == 0)
					System.out.println(counter3);
				// if (counter3==10) break;

			}

			reader3.close();
			// System.out.println(Arrays.toString(inputSentenceEmbedding));

			writer.close();
			
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			double elapsedSeconds = tDelta / 1000.0;
			System.out.println("\ntime:" + elapsedSeconds);

			
			System.out.println("done3");

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}	

	}

	static ArrayList getSentenceEmbeddingsByIDs(String []ids, int flag){

		long tStart = System.currentTimeMillis();
		
		ArrayList idList = new ArrayList();
		for (int i=0;i<ids.length;i++){
			idList.add( MyLib_General.getFieldData(ids[i], 1, "\t"));
	//		System.out.println(ids[i]);
		}
		
		ArrayList SentenceEmbeddingsList = new ArrayList();

		String source = "";
		if (flag ==0) source = "post_sentence_embedding";
		else source = "cmnt_sentence_embedding";
			
		BufferedReader reader2;
		try {
			reader2 = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\Embedding\\" + source));
			String line="";
			while ((line = reader2.readLine()) != null) {
				if (idList.indexOf(MyLib_General.getFieldData(line, 1, "\t"))!=-1){
					SentenceEmbeddingsList.add(line);
				}
			}
			reader2.close();
			return SentenceEmbeddingsList;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long tEnd1 = System.currentTimeMillis();
		long tDelta1 = tEnd1 - tStart;
		double elapsedSeconds1 = tDelta1 / 1000.0;
		System.out.println(elapsedSeconds1);
		


		System.out.println("done");

		return new ArrayList();
				
	}
	static double  semanticSimilarity(String inputSentence){
		
		long tStart = System.currentTimeMillis();
		
		double score=0;
		int counter=0;
		String index="";
		double largest=-1;
		
		double v1[] = calculateEachSentenceEmbedding(inputSentence,0);

	//	System.out.println(Arrays.toString(v1));
		
	//	if (1==1) return 0.0;
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\Embedding\\post_sentence_embedding"));

			String line;

			while ((line = reader.readLine()) != null) {

				String [] vector = MyLib_General.getFieldData(line, 2, "\t").split(" ");
				double [] v2 = new double[200];
				
				for (int i = 0; i < 200; i++) {
					v2[i] = Double.parseDouble(vector[i]);
				}
				

				
				double similarity = calculateInnerProduct(v1, v2);
			//	System.out.println(similarity);
				if (similarity>largest){
					largest =similarity ;
					index = MyLib_General.getFieldData(line, 1, "\t");
					System.out.println(largest + " "+ index+" "+DataOperation.getPostFromId(index));
					
			//		System.out.println(largest + " "+ index+" "+Application.getCommentFromId(index));
				}
				//if (largest>0.8) break;
				
				counter += 1;
				
				 if (counter % 10000 == 0)
					 System.out.println(counter);
			}

			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		
		
		long tEnd = System.currentTimeMillis();
		long tDelta = tEnd - tStart;
		double elapsedSeconds = tDelta / 1000.0;
		System.out.println(elapsedSeconds);
		
	//	System.out.print(largest + " "+ index+" "+Application.getCommentFromId(index));

		System.out.print(largest + " "+ index+" "+DataOperation.getPostFromId(index));
		return score;
	}
	static double[] calculateEachSentenceEmbedding(String inputSentence, int flag) {

		/**
		double v1[] = WordEmbedding.calculateEachSentenceEmbedding("tonight dinner steak");
		double v2[] = WordEmbedding.calculateEachSentenceEmbedding("i must eat the beef");
		double v3[] = WordEmbedding.calculateEachSentenceEmbedding("You do not eat 8 beef");				
		double v4[] = WordEmbedding.calculateEachSentenceEmbedding("western style food");		
		
		System.out.println(WordEmbedding.calculateInnerProduct(v1, v2));

		System.out.println(WordEmbedding.calculateInnerProduct(v1, v3));
		
		System.out.println(WordEmbedding.calculateInnerProduct(v1, v4));
		**/
		
		long tStart = System.currentTimeMillis();
		
		
		double[] inputSentenceEmbedding = new double[200];
		for (int i = 0; i < 200; i++) {
			inputSentenceEmbedding[i] = 0.0;
		}

		String revisedInputSentence = " " + inputSentence.trim() + " ";
		String source_word_embedding="";
		if (flag==0){
			source_word_embedding="post_smaller_word_embedding";
		}else{
			source_word_embedding="cmnt_smaller_word_embedding";
		}
		try {

			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\Embedding\\" + source_word_embedding));
			
			String line;
			int counter = 0;
			int wordCount = inputSentence.trim().split(" ").length;
			
			  while ((line = reader.readLine()) != null) {
			  
		//	  System.out.println(line);
			  String key = MyLib_General.getFieldData(line, 1, " ");
			  
			  String wordEmbeddingList [] =  line.split(" ");
			  
			  if (revisedInputSentence.contains(" "+ key + " ")){
			  
			  for (int i=0;i<200;i++){ 
				  inputSentenceEmbedding[i] += Double.parseDouble(wordEmbeddingList[i+1]); 
			}
			  
			 // System.out.println(line); counter+=1; 
			  if (wordCount<=counter) break; 
			  
			  } // if (counter % 1000 ==0) System.out.println(counter);
			  
			  
			  }
			//

			reader.close();
			// System.out.println(Arrays.toString(inputSentenceEmbedding));
			
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			double elapsedSeconds = tDelta / 1000.0;
			System.out.println(elapsedSeconds);
			
			return inputSentenceEmbedding;

			// System.out.print("done");

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		return inputSentenceEmbedding;

	}

	static String getSimilarWords(String inputString, String filteredQuery) {
//	System.out.print(WordEmbedding.getSimilarWords("university", ""));

		//	System.out.println(WordEmbedding.getSimilarWords("gold", "")); if (1==1) return;
			
		int counter = 0;
		double[] v1 = new double[200];

		try {
			BufferedReader reader = new BufferedReader(
					new FileReader("C:\\Users\\Alan\\Desktop\\caps_support\\glove.6B\\" + sourceWordEmbeddingData));

			String line;

			while ((line = reader.readLine()) != null) {
				String temp = line.split(" ")[0];
				String[] vector = line.split(" ");

				if (temp.equals(inputString)) {

					for (int i = 0; i < 200; i++) {
						v1[i] = Double.parseDouble(vector[i + 1]);
					}

					break;
				}
			}
			reader.close();

		} catch (Exception e) {
			System.out.print("g");
		}

		String[] list = new String[10];
		for (int i = 0; i < 10; i++)
			list[i] = "1";
		int listCount = 0;
		String returnWords = "";

		try {
			BufferedReader reader2 = new BufferedReader(
					new FileReader("C:\\Users\\Alan\\Desktop\\caps_support\\glove.6B\\" + sourceWordEmbeddingData));

			String line;

			double min = -1.0;
			String name = "";

			while ((line = reader2.readLine()) != null) {

				String temp = line.split(" ")[0];
				if (temp.equals(inputString))
					continue;
				if (filteredQuery.contains(temp + " "))
					continue;

				String[] vector = line.split(" ");
				double[] v2 = new double[200];
				name = vector[0];

				for (int i = 0; i < 200; i++) {
					v2[i] = Double.parseDouble(vector[i + 1]);
				}

				double similarity = calculateInnerProduct(v1, v2);

				if (similarity > 0.6) {
					list[listCount++] = similarity + " " + name;
					// System.out.print(name+ " ");
					if (listCount >= 10)
						break;
				}

			}

			reader2.close();
			Arrays.sort(list);
			for (int i = 0; i < listCount; i++) {
				// System.out.println(list[i]);
				returnWords += list[i].split(" ")[1] + " ";
			}
			return returnWords;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";

	}

	static String queryExpansion(String filteredQuery) {
		String expandedQuery = "";

		String[] queryList = filteredQuery.split(" ");

		// System.out.println(getSimilarWords("university"));

		for (int i = 0; i < queryList.length; i++)
			// System.out.println(queryList[i]);
			expandedQuery += getSimilarWords(queryList[i], filteredQuery);
		expandedQuery = MyLib_General.deDup(expandedQuery);
		// System.out.print(expandedQuery);
		// getSimilarWords();
		return expandedQuery;
	}

	static double calculateInnerProduct(double[] v1, double[] v2) {

		double innerProduct = 0;

		double normA = 0.0;
		double normB = 0.0;

		for (int i = 0; i < v1.length; i++) {
			innerProduct += v1[i] * v2[i];
			normA += Math.pow(v1[i], 2);
			normB += Math.pow(v2[i], 2);
		}

		return innerProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
}
