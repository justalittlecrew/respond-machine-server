package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Array;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

import evaluation.Evaluate;
import multi_conversation.MultiConv;
import mylibrary.MyLib_General;
import mylibrary.MyLib_NLP;
import online_searching.BingAPI;
import online_searching.GoogleAPI;
import online_searching.MaxDuplicateWordCount;
import online_searching.Searching;
import web_interface.WebInterface;

//calculates the cosine similarity between two texts / documents etc., (having each word separated by space)
public class BotController {
	// parameter
	static boolean semantics =false;
	static boolean onlineSearching = false;
	static boolean multi_conv = false;
	final static int numberOfThread =3;
	public static int onlineQueryExpansionCount = 3;
	public static boolean showLog = false;
	
	// input path
	public static final String path_repos_cmnt_cn = System.getProperty("user.dir") + "/resources/cmnt/repos-id-cmnt-cn";
	public static final String path_repos_cmnt = System.getProperty("user.dir") + "/resources/cmnt/repos-id-cmnt-en";
	public static final String path_repos_post = System.getProperty("user.dir") + "/resources/post/repos-id-post-en";
	public static final String path_filtered_cmnt_weighted  = System.getProperty("user.dir") + "/resources/cmnt/filteredCommentWithWeighting";
	public static final String path_filtered_post_weighted = System.getProperty("user.dir") + "/resources/post/filteredPostWithWeighting";
	public static final String path_stop_word =  System.getProperty("user.dir") + "/resources/stop_word_list";
	public static final String path_term_Frequency =  System.getProperty("user.dir") + "/resources/cmnt/term_frequency";
	public static final String path_post_term_Frequency =  System.getProperty("user.dir") + "/resources/post/term_frequency";
	
	public static final String path_word_embedding_file =  System.getProperty("user.dir") + "/resources/Embedding/" + "glove.6B.200d.txt";
	public static final String path_Post_Cmnt_SentencesEmbeddingScore  = System.getProperty("user.dir") + "/resources\\Embedding/" + "PostCmntSentencesEmbeddingScore";
	public static final String path_Post_Cmnt_CosineSimilarityScore  = System.getProperty("user.dir") + "/resources/PostCmntCosineSimilarityScore";
	public static final String path_cmnt_smaller_word_embedding = System.getProperty("user.dir") + "/resources/Embedding/" + "cmnt_smaller_word_embedding";
	
	public static final String path_test_label = System.getProperty("user.dir") + "/resources/test/train-label";
	public static final String path_test_post = System.getProperty("user.dir") + "/resources/test/train-id-post-en";
	public static final String path_index_file = System.getProperty("user.dir") + "/resources/indexFile";
	
	public static final String path_server = "C:\\xampp\\htdocs\\";
	
	public static String savingFileName = "untitle";

	
	//static Map<String, String> PostCmntCosineSimilarityScore;
	//static Map<String, String> cmntAndPostEmbeddingScore ;	
	public static Map<String, double[]> wordEmbedding;
	public static String trainingPost="";
//	public static ArrayList experiment = new ArrayList();

	
	public static void initial(){

		System.out.print("Initializing...");
		
	//	PostCmntCosineSimilarityScore = new HashMap<>();
	//	cmntAndPostEmbeddingScore = new HashMap<>();	
		wordEmbedding = new HashMap<>();
		/**
		try {
			BufferedReader reader = new BufferedReader(	new FileReader(path_Post_Cmnt_SentencesEmbeddingScore));
			String line;
			  while ((line = reader.readLine()) != null) {
				  String cmntId = MyLib_General.getFieldData(line, 2, "\t");
				  String postId = MyLib_General.getFieldData(line, 1, "\t");
				  String score = MyLib_General.getFieldData(line, 3, "\t");
				  cmntAndPostEmbeddingScore.put(cmntId,score );
			  }
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		**/
		/**
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(path_Post_Cmnt_CosineSimilarityScore));
			String line;
			  while ((line = reader.readLine()) != null) {
				  String cmntId = MyLib_General.getFieldData(line, 2, "\t");
				  String postId = MyLib_General.getFieldData(line, 1, "\t");
				  String score = MyLib_General.getFieldData(line, 3, "\t");
				 // PostCmntCosineSimilarityScore.put(postId + "\t"+ cmntId,score );
				  PostCmntCosineSimilarityScore.put(cmntId,score );
			  }
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		**/
		
		
		try {

			BufferedReader reader = new BufferedReader(new FileReader(path_cmnt_smaller_word_embedding));
			String line;
			  while ((line = reader.readLine()) != null) {
			 
			  String wordKey = MyLib_General.getFieldData(line, 1, " ");
			  
			  String wordEmbeddingList [] =  line.split(" ");
			  double [] wordVector = new double [200];
			  
			  for (int i=0;i<200;i++){ 
				  wordVector[i] += Double.parseDouble(wordEmbeddingList[i+1]); 
			  }

			  
			  wordEmbedding.put(wordKey,wordVector );
	  
			  }
			  reader.close();
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		if (BotController.showLog)
		System.out.println("map size" + wordEmbedding.size());

		MultiConv.initial();
		

		
		try {
			BufferedReader reader = new BufferedReader(	new FileReader(path_filtered_cmnt_weighted));
		//	PrintWriter writer = new PrintWriter("resources//indexFile");
			ArrayList<PrintWriter> writerList = new ArrayList<PrintWriter>();
			// new PrintWriter("resources\\cmnt\\filtered_cmnt_withoutid");
			 
			for (int i=0;i<numberOfThread;i++){
				try {
					writerList.add(new PrintWriter("resources\\cmnt\\filteredCommentWithWeighting_" + i));
				
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			String line;
			int counter=0;
			  while ((line = reader.readLine()) != null) {
				  writerList.get(counter++%numberOfThread).println(line);
			  }
			  reader.close();
				for (int i=0;i<numberOfThread;i++){
					writerList.get(i).close();
				}
				
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		//if (Application.showLog)
		System.out.println("done!");
	}
	public static String[] threadAgent(String queryString, String callFunction){
		   
	    Date start = new Date();  
	    
	    String filteredQuery =  BotController.filterStopWordFromQuery(queryString);
		filteredQuery = MyLib_General.deDup(filteredQuery);
		
		if (multi_conv==true && !callFunction.startsWith("MultiConv" + "\t" )){ // call by ming program
		
			String returnList [] = {MultiConv.caching(filteredQuery)}; 
			if (returnList[0].trim() !=""){
				;
				//MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + returnList[0]);	
				//filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(resultList[0], 2, "\t")
				return returnList;
			}
		}
	
		//   //MultiConv
		
	    ArrayList<Threads> threadList = new  ArrayList<Threads>();
      
	    for (int i=0;i<numberOfThread;i++){
	    	threadList.add(new Threads(i + "", queryString, callFunction));
	    }
  
	    for (int i=0;i<numberOfThread;i++){
	    	threadList.get(i).start();
	    }
      

	    
	    try {
	        for (int i=0;i<numberOfThread;i++){
		    	threadList.get(i).t.join();
		    }
			
			String[] combineResultList = new String[10*numberOfThread];
			int counter =0;
			for (int k=0;k<numberOfThread;k++){
				for (int i =0;i<10;i++){
					combineResultList[counter++]=threadList.get(k).resultList[i];
				}
			}
		
			
			Arrays.sort(combineResultList,Collections.reverseOrder());

		    if (showLog)
		    	System.out.println(Arrays.toString(combineResultList ) );
			
			if (Double.parseDouble(MyLib_General.getFieldData(combineResultList[9], 1, "\t") )<1.05   && onlineSearching == true && semantics==true){

			    if (showLog)
				System.out.println("online and semantics");
				//	combineResultList =method_justCommentForReuse(Searching.search(filteredQuery),"justComment", "result");
				String expandedQuery = Searching.search(filteredQuery);
				
			//	combineResultList =method_justCommentForReuse( expandedQuery,"justComment", "result");
				combineResultList =method_justComment( expandedQuery,"-1", "agent");
				
			    
			}else if(Double.parseDouble(MyLib_General.getFieldData(combineResultList[9], 1, "\t") )<0.46571182916616827   && onlineSearching == true && semantics==false){

			    if (showLog)
			    	System.out.println("online");
				String expandedQuery = Searching.search(filteredQuery);
				//combineResultList =method_justCommentForReuse(expandedQuery,"justComment", "result");
				combineResultList =method_justComment( expandedQuery,"-1", "agent");
			}
			
			String[] returnList = new String[10];
			
			for (int i=0;i<returnList.length;i++) 
				returnList[i] = MyLib_General.getFieldData(combineResultList[i], 2, "\t");
			
			
		    Date end = new Date();  
	        
		    long difference = end.getTime() - start.getTime();  
		    
		    if (showLog)
		    	System.out.println ("This whole process took: " + difference/1000 + " seconds.");  

		  
		    MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + returnList[0]);					
			
			return returnList;
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		return new String[10];
	}
	/**
	public static String [] LCS(String inputQuery){
		  boolean semantics =true;
			
		  savingFileName = "LCS";
		//	while (true) {

				String[] top10Comment = new String[20];
				Double[] top10CommentScore = new Double[20];	
				
				for (int i = 0; i < top10CommentScore.length; i++)	top10CommentScore[i] = 0.0;
				
				
				for (int i = 0; i < top10Comment.length; i++)	top10Comment[i] = "";
			
				Double thresholdForComment = 0.0;		
				
				String queryString = "";
				
				if (inputQuery.equals("")){
					Scanner scanner = new Scanner(System.in);
					System.out.println("please enter a new query: ");
					queryString = scanner.nextLine().trim();
				}
				else{
					queryString = inputQuery;
				}
			
				String filteredQuery =  Preprocessing.filterStopWordFromQuery(queryString);
		
				double [] queryWeighting = getQueryWeighting(filteredQuery);
				
				for (int i =0;i<queryWeighting.length;i++)
					System.out.print(queryWeighting[i]+ " ");
				System.out.println();
				
				
				filteredQuery = MyLib_General.deDup(filteredQuery);
				
				System.out.println("filter query: \n" + filteredQuery);
				System.out.println();
				

				
				System.out.println("top 5 responses from computer (rank from high to low): ");
			
				
				String queryWithoutPunctuation= Preprocessing.removePunctuation(inputQuery);
				
				try {
					BufferedReader reader2 = new BufferedReader(
					//		new FileReader(System.getProperty("user.dir") + "\\repos-id-cmnt-en"));
							new FileReader(System.getProperty("user.dir") + "\\filteredCommentWithWeighting"));

					String line;
				
					System.out.println(inputQuery);
					long counter2 = 0;
					int longestCount=0;
					String longestComment="";
					while ((line = reader2.readLine()) != null) {
		
						String lineWithoutPunctuation= Preprocessing.removePunctuation(line);
						//System.out.println(lineWithoutPunctuation);
					
					//	counter2+=1;
			//			if (counter2>1000000) break;
						

					}
					System.out.println("longestCount:" + longestCount);
					System.out.println("longestComment" + longestComment);
						
				} catch (Exception e) {
					final StringWriter sw = new StringWriter();
					final PrintWriter pw = new PrintWriter(sw, true);
					e.printStackTrace(pw);
					System.out.println(sw.getBuffer().toString());
					sw.getBuffer().toString();
				}



				
				return new String[10];	
	}
	**/
	/**
	static String [] method_CommentAndPost(String inputQuery){
		
		
		double postQueryWeighting = 0.5;
		double postCmntWeighting =1.0;
		double cmntQueryWeighting = 1.5;
		
		boolean penlityForLongSentence = true;
		
		String[] top10Post = new String[50];
		Double[] top10PostScore = new Double[50];			

	//	String[] top10Comment = new String[20];
	//	Double[] top10CommentScore = new Double[20];	
		
		for (int i = 0; i < top10PostScore.length; i++) {
			top10PostScore[i] = 0.0;
		//	top10CommentScore[i] = 0.0;
		}
		
		for (int i = 0; i < top10PostScore.length; i++) {
			top10Post[i] = "";
		//	top10Comment[i] = "";3
		}
		

		Double thresholdForPost = 0.0;
		Double thresholdForComment = 0.0;		
		
		String queryString = "";

		
		if (inputQuery.equals("")){
			Scanner scanner = new Scanner(System.in);
			System.out.println("please enter a new query: ");
			queryString = scanner.nextLine().trim();
		}
		else{
			queryString = inputQuery;
		}

		//System.out.println("new query: " + queryString);
		String filteredQuery =  Preprocessing.filterStopWordFromQuery(queryString);

		//ArrayList queryList = MyLib_NLP.getVectorString(filteredQuery, "");
		double [] queryWeighting = DataOperation.getQueryWeightingFromPost(filteredQuery);
		
		
	//	for (int i =0;i<queryWeighting.length;i++)	System.out.print(queryWeighting[i]+ " ");
		System.out.println();
		
		
		filteredQuery = MyLib_General.deDup(filteredQuery);
		
		String [] queryStringList = filteredQuery.split(" ");
		double [] querySentenceEmbedding = new double [200];
		
		for (int i=0;i<queryStringList.length;i++){
			double [] wordVector = wordEmbedding.get(queryStringList[i]);
		//	System.out.println(Arrays.toString(wordVector));
			if (wordVector ==null) continue;
			for (int j=0;j<wordVector.length;j++) 
				querySentenceEmbedding[j] +=wordVector[j];
		}
	//	System.out.println(Arrays.toString(querySentenceEmbedding));
		
		System.out.println("filter query: \n" + filteredQuery);	
		System.out.println();
		System.out.println("top 5 responses from computer (rank from high to low): ");
	
		long counter2 = 0;

		try {
			BufferedReader reader2 = new BufferedReader(
					new FileReader(path_filtered_post_weighted));

			String line;

			while ((line = reader2.readLine()) != null) {

				counter2 += 1;// 5648128 comment

				String weighting = MyLib_General.getFieldData(line, 3, "\t");
				
				String [] weightingList = weighting.split(" ");
				double [] weightingarray = new double [weightingList.length];
			
				
				for (int i =0;i<weightingList.length;i++){
					if (weightingList[i].trim().equals("")) continue;
					weightingarray[i] = Double.parseDouble(weightingList[i]); 
				}
				
				double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
				//sim_score = sim_score/weightingarray.length;

				if (penlityForLongSentence){
					if (weightingarray.length>=10){
						sim_score *= Math.sqrt(3.0/weightingarray.length);
						}
				}
				if (sim_score > thresholdForComment) {
			
					// check duplicated
					boolean duplicated =false;
					
					for (int i=0;i<top10PostScore.length;i++){
						if (top10Post[i].equals(line)){
							duplicated = true;
						}
					}
					if (duplicated) continue;
					
					double minimumScore=1;
					int  minimumIndex=0;
					for (int i=0;i<top10PostScore.length;i++){
						if (minimumScore> top10PostScore[i]){
							minimumScore = top10PostScore[i];
							minimumIndex =i;
							
						}
					}

					top10PostScore[minimumIndex] = sim_score;
					top10Post[minimumIndex] = line;
					
					thresholdForComment= sim_score;
					for (int i=0;i<top10PostScore.length;i++){
					//	System.out.println(top10PostScore[i]);
						if (thresholdForComment > top10PostScore[i]){
							thresholdForComment = top10PostScore[i];
						}
					}

				}
			}
			reader2.close();

			//Arrays.sort(top10PostScore);
			
			String [] postScoreAndContent = new String[top10PostScore.length];
			for (int i=0;i<postScoreAndContent.length;i++)
				postScoreAndContent[i] = top10PostScore[i]+ "\t"+ top10Post[i];
			
			Arrays.sort(postScoreAndContent);
			
			for (int i=0;i<postScoreAndContent.length;i++)	System.out.println(postScoreAndContent[i]);
			
			System.out.println("cp0");
			Map<String, double[]> postIdAndsentenceEmbedding = new HashMap<>();
			ArrayList SentenceEmbeddingsList = WordEmbedding.getSentenceEmbeddingsByIDs(top10Post,0);

			double v1[] = WordEmbedding.calculateEachSentenceEmbedding(filteredQuery,0);
			String[] resultList = new String[SentenceEmbeddingsList.size()];
			
			for (int i=0;i<SentenceEmbeddingsList.size();i++){
				double [] v2 = new double[200];
				
				String [] vector = MyLib_General.getFieldData(SentenceEmbeddingsList.get(i).toString(), 2, "\t").split(" ");
				for (int j = 0; j < 200; j++) {
					v2[j] = Double.parseDouble(vector[j]);
					
					double score = WordEmbedding.calculateInnerProduct(v1, v2);
					resultList[i]= MyLib_General.getFieldData(score +" " + SentenceEmbeddingsList.get(i).toString(), 1, "\t") ;
				}
				postIdAndsentenceEmbedding.put(MyLib_General.getFieldData(SentenceEmbeddingsList.get(i).toString(), 1, "\t"), v2);
				//System.out.println(Arrays.toString(v2));
			}
			
			
			//Arrays.sort(resultList);
		
			String [] combineResultList  = new String [resultList.length];
			for (int i=0;i<top10PostScore.length;i++){
				
				String id = MyLib_General.getFieldData(top10Post[i], 1, "\t");
				double cosScore = top10PostScore[i];
				double semanScore = 0.0;
				
				for (int j=0;j<resultList.length;j++){
					if (MyLib_General.getFieldData(resultList[j], 2, " " ).equals(id)){
						semanScore = Double.parseDouble(MyLib_General.getFieldData(resultList[j], 1, " " ));
						break;
					}
				}
			//	combineResultList[i]= (cosScore+semanScore)/2 + " " + id;
				combineResultList[i]= cosScore + " " + id;
			}
			
			Arrays.sort(combineResultList);
		//	for( int i=combineResultList.length-1;i>=combineResultList.length-10;i--) System.out.println(combineResultList[i] + " "+ getPostFromId(MyLib_General.getFieldData(combineResultList[i], 1, "\t").split(" ")[1] ));
		//	System.out.println();
		//	System.out.println(getPostFromId(MyLib_General.getFieldData(combineResultList[99], 1, "\t").split(" ")[1] ));

			// ================================ end select post
			
			
			
			
			Map<String, String> postIdAndScore = new HashMap<>();
			Map<String, String> cmntIdAndpostId = new HashMap<>();
			Map<String, String> cmntIdAndQueryScore = new HashMap<>();					

			
			for (int i=combineResultList.length-1;i>=combineResultList.length-10;i--){		
				postIdAndScore.put(MyLib_General.getFieldData(combineResultList[i], 2, " "), MyLib_General.getFieldData(combineResultList[i], 1, " "));
			//		System.out.println(combineResultList[i]);
			}						

			try {
				BufferedReader reader = new BufferedReader(
						new FileReader(path_index_file));

				String line1;

				while ((line1 = reader.readLine()) != null) {
					String postId = line1.split("\t")[0];
					if (postIdAndScore.containsKey(postId)){
						cmntIdAndpostId.put(line1.split("\t")[1],postId );					
					}
				}
				reader.close();

			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}
			
			
			System.out.println("postIdAndScore:" + postIdAndScore.size());
			System.out.println("cmntIdAndpostId:" + cmntIdAndpostId.size());
			
			
			
			
			
			
			
			
			
			
			
			
			
			String [] temp = new String[cmntIdAndpostId.size()];
			int selectedCommentCntr =0;
			try {
				BufferedReader reader = new BufferedReader(
						new FileReader(path_filter_cmnt_weighted));

				String line1;

				while ((line1 = reader.readLine()) != null) {
					
					if (selectedCommentCntr>=temp.length) break;
					
					String cmntId = line1.split("\t")[0];
					if (cmntIdAndpostId.containsKey(cmntId)){
						temp[selectedCommentCntr] = line1;		
						selectedCommentCntr+=1;
					}
				}
				reader.close();

			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}

			
			
			
			String [] selectedComment = new String[selectedCommentCntr];   // id\t weighting
			for (int i=0;i<selectedCommentCntr;i++){
				selectedComment[i]= temp[i];
			//	System.out.println(cmntIdAndpostId.get(MyLib_General.getFieldData(selectedComment[i], 1, "\t"))+ " "+	selectedComment[i]);
			}
			

			
			String [] commentPostScore = new String[selectedComment.length];  //<cmnt id, score>
		//	System.out.println("selectedcomment length " + selectedComment.length);
			
			for (int i=0;i<selectedComment.length;i++){

				String weighting = MyLib_General.getFieldData(selectedComment[i], 3, "\t");
				
				
				String [] weightingList = weighting.split(" ");
				double [] weightingarray = new double [weightingList.length];
			
				
				for (int j=0;j<weightingList.length;j++){
					if (weightingList[j].trim().equals("")) continue;
					weightingarray[j] = Double.parseDouble(weightingList[j]); 
				}
				
				double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(selectedComment[i], 2, "\t")),weightingarray);

				
				String [] cmntStringList = MyLib_General.deDup(MyLib_General.getFieldData(selectedComment[i], 2, "\t")).split(" ");
				double [] cmntSentenceEmbedding = new double [200];
				
				for (int j=0;j<cmntStringList.length;j++){
					double [] wordVector = wordEmbedding.get(cmntStringList[j]);
				//	System.out.println(Arrays.toString(wordVector));
					if (wordVector ==null) continue;
					for (int k=0;k<wordVector.length;k++) 
						cmntSentenceEmbedding[k] +=wordVector[k];
				}
				
				double semantic_score = WordEmbedding.calculateInnerProduct(querySentenceEmbedding, cmntSentenceEmbedding);
				
				
				cmntIdAndQueryScore.put(MyLib_General.getFieldData(selectedComment[i], 1, "\t"), (sim_score + semantic_score)/2 + "");

				commentPostScore[i] = MyLib_General.getFieldData(selectedComment[i], 1, "\t") + "\t" + (sim_score + semantic_score)/2;
			//	System.out.println(commentPostScore[i]);
			}
			System.out.println("cp1");
			
		

			String [] finalResultComment = new String[selectedComment.length];
			for (int i=0;i<selectedComment.length;i++){
				String cmntId = MyLib_General.getFieldData(selectedComment[i],1 , "\t");
				double postAndQueryScore = Double.parseDouble(postIdAndScore.get(cmntIdAndpostId.get(cmntId)));
				double cmntAndQueryScore = Double.parseDouble(cmntIdAndQueryScore.get(cmntId));
				//double cmntAndPostScore = Double.parseDouble(cmntIdAndPostScore.get(cmntId));
				
				String cmntAndPostSemanScore = cmntAndPostEmbeddingScore.get(cmntId);
				String cmntAndPostSimScore = PostCmntCosineSimilarityScore.get(cmntId);	
				
				if (cmntAndPostSemanScore==null){
					System.out.println("cmntAndPostEmbeddingScore" + cmntId + "null" );
					cmntAndPostSemanScore = "0.0";
				}
				if (cmntAndPostSimScore==null){
					System.out.println("PostCmntCosineSimilarityScore" + cmntId + "null" );
					cmntAndPostSimScore = "0.0";
				}				
				double cmntAndPostScore = (Double.parseDouble(cmntAndPostSemanScore) +  Double.parseDouble(cmntAndPostSimScore))/2;
						
				double totalScore = postAndQueryScore*postQueryWeighting+cmntQueryWeighting*cmntAndQueryScore+postCmntWeighting*cmntAndPostScore;
				System.out.println(cmntId+ " "+postAndQueryScore+ " "+ cmntAndQueryScore + " " +cmntAndPostScore );
				finalResultComment [i] = totalScore + " " + cmntId;

			}
			Arrays.sort(finalResultComment);
			String [] returnResultList = new String [10];
			int returnResultListCounter=0;
			for (int i=finalResultComment.length-1;i>=0;i--){
				System.out.println(finalResultComment[i]+ " " + DataOperation.getCommentFromId(MyLib_General.getFieldData(finalResultComment[i], 2, " "), 0));
				returnResultList[returnResultListCounter++] = MyLib_General.getFieldData(finalResultComment[i], 2, " ");
				if (returnResultListCounter==10) break;
			}
			
			
			return returnResultList;
				
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		System.out.println();
		

		
	
		return new String[10];
		
	}
	**/
/**
static String [] method_My(String inputQuery){
	  boolean semantics =true;
		
	  savingFileName = "MyMethod";
	//	while (true) {


		

			String[] top10Comment = new String[20];
			Double[] top10CommentScore = new Double[20];	
			
			for (int i = 0; i < top10CommentScore.length; i++) {
	
				top10CommentScore[i] = 0.0;
			}
			
			for (int i = 0; i < top10Comment.length; i++) {
	
				top10Comment[i] = "";
			}

			

			Double thresholdForPost = 0.0;
			Double thresholdForComment = 0.0;		
			
			String queryString = "";

			
			if (inputQuery.equals("")){
				Scanner scanner = new Scanner(System.in);
				System.out.println("please enter a new query: ");
				queryString = scanner.nextLine().trim();
			}
			else{
				queryString = inputQuery;
			}
		

			//System.out.println("new query: " + queryString);
			String filteredQuery =  Preprocessing.filterStopWordFromQuery(queryString);
	
			
			//ArrayList queryList = MyLib_NLP.getVectorString(filteredQuery, "");
			double [] queryWeighting = getQueryWeighting(filteredQuery);
			
		
			
			//if (largestWeightingWeightMore){
			//	int largeIndex=-1;
			////	double largeValue=0;
			//	for (int i =0;i<queryWeighting.length;i++){
			//		if (queryWeighting[i]>largeValue){
			//			largeValue=queryWeighting[i];
			//			largeIndex=i;
			//		}
			//	}
			//	queryWeighting[largeIndex]*=2;
			//}
		
			for (int i =0;i<queryWeighting.length;i++)
				System.out.print(queryWeighting[i]+ " ");
			System.out.println();
			
			
			filteredQuery = MyLib_General.deDup(filteredQuery);
			
			System.out.println("filter query: \n" + filteredQuery);	
		//	double leastScore=10.0;
			
		//	for (int i =0;i<queryWeighting.length;i++){
		//		if (queryWeighting[i] < leastScore)
		//			leastScore=queryWeighting[i];
		//	}
			//leastScore-=0.5;
			System.out.println();
			

		//	if (1==1) return "";
			
			System.out.println("top 5 responses from computer (rank from high to low): ");
		

			long counter2 = 0;

			try {
				BufferedReader reader2 = new BufferedReader(
						new FileReader(System.getProperty("user.dir") + "\\filteredCommentWithWeighting"));

				String line;

				while ((line = reader2.readLine()) != null) {

					counter2 += 1;// 5648128 comment

					//if (counter2 > 1000000)
					//	break;

				
					
					String weighting = MyLib_General.getFieldData(line, 3, "\t");
					
					
					String [] weightingList = weighting.split(" ");
					double [] weightingarray = new double [weightingList.length];
				
					
					for (int i =0;i<weightingList.length;i++){
						if (weightingList[i].trim().equals("")) continue;
						weightingarray[i] = Double.parseDouble(weightingList[i]); 
					}
					
				//	if (1==1) return "";
			//		double sim_score = MyLib_NLP.cosine_Similarity(expandedQuery,newQueryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
					double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
	

					if (sim_score > thresholdForComment) {

					
						
						// check duplicated
						boolean duplicated =false;
						
						for (int i=0;i<top10CommentScore.length;i++){
							if (top10Comment[i].equals(line)){
								duplicated = true;
							}
						}
						if (duplicated) continue;
						
						double minimumScore=1;
						int  minimumIndex=0;
						for (int i=0;i<top10CommentScore.length;i++){
							if (minimumScore> top10CommentScore[i]){
								minimumScore = top10CommentScore[i];
								minimumIndex =i;
								
							}
						}

						top10CommentScore[minimumIndex] = sim_score;
						top10Comment[minimumIndex] = line;
						
						thresholdForComment= sim_score;
						for (int i=0;i<top10CommentScore.length;i++){
						//	System.out.println(top10PostScore[i]);
							if (thresholdForComment > top10CommentScore[i]){
								thresholdForComment = top10CommentScore[i];
							}
						}

					}
				}
				reader2.close();

		
				
				
				String [] cmntScoreAndContent = new String[top10CommentScore.length];
				for (int i=0;i<cmntScoreAndContent.length;i++)
					cmntScoreAndContent[i] = top10CommentScore[i]+ "\t"+ top10Comment[i];	
				
				
				
				
				if (semantics){

					double v1[] = WordEmbedding.calculateEachSentenceEmbedding(filteredQuery,0);
					
					

					
			//		Arrays.sort(cmntScoreAndContent);
					
					String resultList [] = new String [cmntScoreAndContent.length];

					for (int i=0;i<cmntScoreAndContent.length;i++){
						double [] cmntSentenceEmbedding = new double [200];
						String [] wordVectorString = MyLib_General.getFieldData(cmntScoreAndContent[i], 3, "\t").split(" ");

						//System.out.println(Arrays.toString(wordVectorString));
					
						
						for (int j=0;j<wordVectorString.length;j++){
							double [] wordVector = wordEmbedding.get(wordVectorString[j]);
							if (wordVector ==null) continue;
							for (int k=0;k<wordVector.length;k++) 
								cmntSentenceEmbedding[k] +=wordVector[k];
						}


						double semantic_score = WordEmbedding.calculateInnerProduct(v1, cmntSentenceEmbedding);
					//	System.out.println(semantic_score+ " "+ cmntScoreAndContent[i])			;	
						double totalScore = semantic_score + Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[i],1, "\t"));
						resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t");
						
					}
					Arrays.sort(resultList,Collections.reverseOrder());

					String [] returnList = new String [10];
					for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(resultList[i], 2, "\t");
				
					return returnList;
				}
				else{
					
					Arrays.sort(cmntScoreAndContent,Collections.reverseOrder());
					
					String returnList [] = new String[10];
					for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(cmntScoreAndContent[i], 2, "\t");
							
					return returnList;
				}

					
			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}



			
			return new String[10];
		
	}
	
**/
static String[] method_justRetrievePostComment(String inputQuery){
		
		String[] top10Post = new String[10];
		Double[] top10PostScore = new Double[10];			

		for (int i = 0; i < top10PostScore.length; i++) {
			top10PostScore[i] = 0.0;
		//	top10CommentScore[i] = 0.0;
		}
		
		for (int i = 0; i < top10PostScore.length; i++) {
			top10Post[i] = "";
		//	top10Comment[i] = "";3
		}
		

		Double thresholdForPost = 0.0;
		Double thresholdForComment = 0.0;		
		
		String queryString = "";
		
		if (inputQuery.equals("")){
			Scanner scanner = new Scanner(System.in);
			System.out.println("please enter a new query: ");
			queryString = scanner.nextLine().trim();
		}
		else{
			queryString = inputQuery;
		}
	

		//System.out.println("new query: " + queryString);
		String filteredQuery =  BotController.filterStopWordFromQuery(queryString);

		//ArrayList queryList = MyLib_NLP.getVectorString(filteredQuery, "");
		double [] queryWeighting = getQueryWeightingFromPost(filteredQuery);
		
		
		//for (int i =0;i<queryWeighting.length;i++)
		//	System.out.print(queryWeighting[i]+ " ");
		//System.out.println();
		
		
		filteredQuery = MyLib_General.deDup(filteredQuery);
		
		System.out.println("filter query: \n" + filteredQuery);	
		System.out.println();
		System.out.println("top 5 responses from computer (rank from high to low): ");
	
		long counter2 = 0;

		try {
			BufferedReader reader2 = new BufferedReader(
					new FileReader(path_filtered_post_weighted));

			String line;

			while ((line = reader2.readLine()) != null) {

				counter2 += 1;// 5648128 comment

				String weighting = MyLib_General.getFieldData(line, 3, "\t");
				
				String [] weightingList = weighting.split(" ");
				double [] weightingarray = new double [weightingList.length];
			
				
				for (int i =0;i<weightingList.length;i++){
					if (weightingList[i].trim().equals("")) continue;
					weightingarray[i] = Double.parseDouble(weightingList[i]); 
				}
				
				double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
				if (weightingarray.length>=3){
						sim_score *= Math.sqrt(3.0/weightingarray.length);
				}
					

				if (sim_score > thresholdForComment) {
			
					// check duplicated
					boolean duplicated =false;
					
					for (int i=0;i<top10PostScore.length;i++){
						if (top10Post[i].equals(line)){
							duplicated = true;
						}
					}
					if (duplicated) continue;
					
					double minimumScore=1;
					int  minimumIndex=0;
					for (int i=0;i<top10PostScore.length;i++){
						if (minimumScore> top10PostScore[i]){
							minimumScore = top10PostScore[i];
							minimumIndex =i;
							
						}
					}

					top10PostScore[minimumIndex] = sim_score;
					top10Post[minimumIndex] = line;
					
					thresholdForComment= sim_score;
					for (int i=0;i<top10PostScore.length;i++){
					//	System.out.println(top10PostScore[i]);
						if (thresholdForComment > top10PostScore[i]){
							thresholdForComment = top10PostScore[i];
						}
					}

				}
			}
			reader2.close();

			//Arrays.sort(top10PostScore);
			
			String [] postScoreAndContent = new String[top10PostScore.length];
			for (int i=0;i<postScoreAndContent.length;i++)
				postScoreAndContent[i] = top10PostScore[i]+ "\t"+ top10Post[i];
			
			Arrays.sort(postScoreAndContent);
			
		//	for (int i=postScoreAndContent.length-1;i>0;i--)	{
		//		System.out.println(postScoreAndContent[i]);
			//}
			
			
			System.out.println("Comment:");
			for (int i=postScoreAndContent.length-1;i>0;i--)	{
				System.out.println(postScoreAndContent[i]);
				
				ArrayList commentIdList = DataOperation.getCommentIdsFromPostId(MyLib_General.getFieldData(postScoreAndContent[i], 2, "\t"));

				String [] returnList = new String[10];
				for (int j=0;j<commentIdList.size();j++){
					returnList[j]=commentIdList.get(j).toString();
					if (j==9) return returnList;
					//return commentIdList.get(j).toString();
					//if (1==1) return"";
				}
				
				if (1==1) continue;
			}
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		return new String[10];
		
	}
	

 public static String [] method_justComment(String inputQuery, String multithreading, String hostCall) {

	  savingFileName = "testing_JustComment";
	 
			String[] top10Comment = new String[20];
			Double[] top10CommentScore = new Double[20];	
			
			for (int i = 0; i < top10CommentScore.length; i++) {
				top10CommentScore[i] = 0.0;
			}
			
			for (int i = 0; i < top10Comment.length; i++) {
				top10Comment[i] = "";
			}

			Double thresholdForComment = 0.0;		
			
			String queryString = inputQuery;
			
			String filteredQuery =  BotController.filterStopWordFromQuery(queryString);
	
			double [] queryWeighting = getQueryWeighting(filteredQuery);
				

			filteredQuery = MyLib_General.deDup(filteredQuery);

			if (MyLib_General.getFieldData(hostCall, 1, "\t").equals("MultiConv")){
				String newQueryString = MyLib_General.getFieldData(hostCall, 2, "\t").trim() + " ";
				String queryHistory = MyLib_General.getFieldData(hostCall, 3, "\t").trim()+ " ";
				
				String [] temp = filteredQuery.split(" ");
				
				for (int i =0;i<temp.length;i++){
					if (queryHistory.contains(temp[i]+" ")){
					
						queryWeighting[i]*=0.5;
						if (showLog)
							System.out.println("penalty: " + temp[i]);
					}
				}
			}			

			if (showLog)
				System.out.println("filter query: \n" + filteredQuery);
						
			

			long counter2 = 0;

			try {

				String threadId = MyLib_General.getFieldData(multithreading, 1, "\t");
				String fileIndex="";
				if (!threadId.equals("-1")) fileIndex ="_"+threadId;
				
				BufferedReader reader2 = new BufferedReader(
						new FileReader(path_filtered_cmnt_weighted + fileIndex));

				String line;
	
				
				while ((line = reader2.readLine()) != null) {

					counter2 += 1;// 5648128 comment
					
					// don't return deplicated
					if (MyLib_General.getFieldData(hostCall, 1, "\t").equals("MultiConv")){
						ArrayList temp = MultiConv.getCommentIdHistory();
						if (temp.indexOf(MyLib_General.getFieldData(line, 1, "\t"))!=-1) continue;						
					}


					
				
					
					String weighting = MyLib_General.getFieldData(line, 3, "\t");
					
					String [] weightingList = weighting.split(" ");
					double [] weightingarray = new double [weightingList.length];
				
					
					for (int i =0;i<weightingList.length;i++){
						if (weightingList[i].trim().equals("")) continue;
						weightingarray[i] = Double.parseDouble(weightingList[i]); 
					}
					
					
			double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
	

					if (sim_score > thresholdForComment) {//insert this comment

						// start check duplicated
						boolean duplicated =false;
						
						for (int i=0;i<top10CommentScore.length;i++)
							if (top10Comment[i].equals(line))
								duplicated = true;
						if (duplicated) continue;
						// end check duplicated
						
						//==========start to replace the worst candidate comment
						double minimumScore=1;
						int  minimumIndex=0;
						for (int i=0;i<top10CommentScore.length;i++){
							if (minimumScore> top10CommentScore[i]){
								minimumScore = top10CommentScore[i];
								minimumIndex =i;
							}
						}
						top10CommentScore[minimumIndex] = sim_score;
						top10Comment[minimumIndex] = line;
						thresholdForComment= sim_score;
						//==========end to replace the worst candidate comment
						
						//==========start to find the minimum score as threshold						
						for (int i=0;i<top10CommentScore.length;i++)
							if (thresholdForComment > top10CommentScore[i])
								thresholdForComment = top10CommentScore[i];
						//==========end to find the minimum score as threshold							
						
					}
				}
				reader2.close();

				String [] cmntScoreAndContent = new String[top10CommentScore.length];
				for (int i=0;i<cmntScoreAndContent.length;i++)
					cmntScoreAndContent[i] = top10CommentScore[i]+ "\t"+ top10Comment[i];	
				
				String [] returnList = new String[10];
				String [] resultList = cmntScoreAndContent;
				
				if (semantics){


					
					double v1[] = Embedding.calculateEachSentenceEmbedding(filteredQuery,0);
					
					 resultList = new String [cmntScoreAndContent.length];

					for (int i=0;i<cmntScoreAndContent.length;i++){
						double [] cmntSentenceEmbedding = new double [200];
						String [] wordVectorString = MyLib_General.getFieldData(cmntScoreAndContent[i], 3, "\t").split(" ");

						
						for (int j=0;j<wordVectorString.length;j++){
							double [] wordVector = wordEmbedding.get(wordVectorString[j]);
							if (wordVector ==null) continue;
							for (int k=0;k<wordVector.length;k++) 
								cmntSentenceEmbedding[k] +=wordVector[k];
						}


						double semantic_score = Embedding.calculateInnerProduct(v1, cmntSentenceEmbedding);
					//	System.out.println(semantic_score+ " "+ cmntScoreAndContent[i])			;	
						double totalScore = semantic_score + Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[i],1, "\t"));
					//	resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t");
						resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t") + "\t" + semantic_score ;
						
					
					}
					Arrays.sort(resultList,Collections.reverseOrder());
					if (showLog)
						System.out.println(Arrays.toString(resultList));
				//	returnList = new String [11];
					for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(resultList[i], 2, "\t");
				
				//	returnList[10] = MyLib_General.getFieldData(resultList[0], 1, "\t") + " " +MyLib_General.getFieldData(resultList[9], 1, "\t")  ;
				//	returnList[10] =  MyLib_General.getFieldData(resultList[0], 3, "\t");

					
					
				//	if (Double.parseDouble(MyLib_General.getFieldData(resultList[9], 1, "\t") )<1.0   && onlineSearching == true){
					
						//returnList = method_justCommentForReuse(Searching.search(filteredQuery),"justComment");
				//		resultList =method_justCommentForReuse(Searching.search(filteredQuery),"justComment", "result");
							
				//	}
				//	else{
						
					//	MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(resultList[0], 2, "\t"));					
					//	System.out.println(resultList[0]);
						//return returnList;
				//	}
					
				}
				else{// not semantics
				
					if (Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[9], 1, "\t") )<0.46571182916616827   && onlineSearching == true){
					//	System.out.println("online");
					//	resultList = method_justCommentForReuse(Searching.search(filteredQuery),"justComment", "result");
						
					}
					else{
						Arrays.sort(cmntScoreAndContent,Collections.reverseOrder());
					
						returnList = new String[11];
					for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(cmntScoreAndContent[i], 2, "\t");
							
					returnList[10] = MyLib_General.getFieldData(cmntScoreAndContent[0], 1, "\t") + " " +MyLib_General.getFieldData(cmntScoreAndContent[9], 1, "\t") ;
				
					resultList =cmntScoreAndContent;
					return resultList;
					}
					
					
				}

				
				
				return resultList;
					
			} catch (Exception e) {
				final StringWriter sw = new StringWriter();
				final PrintWriter pw = new PrintWriter(sw, true);
				e.printStackTrace(pw);
				System.out.println(sw.getBuffer().toString());
				sw.getBuffer().toString();
			}

			return new String[10];
	}
 public static void controller(String[] args) {

	
		
		/**
		// =============================== preprocess start	
			Preprocessing.processingStepOne();
			Preprocessing.processingStepTwo();	
			if(1==1) return;
		// =============================== preprocess end			
		**/
			
	 
		///**
		//================================= start
		initial();
		System.out.println("");
		while(true){
			Scanner scanner = new Scanner(System.in);
			System.out.println("press 1: command line.\npress 2: web interface");
			System.out.print("Enter option [1-2] : ");

			String input = scanner.nextLine().trim();
			
			int inputCode = Integer.parseInt(input);
			if (inputCode==1) {
				semantics = false;
				onlineSearching=false;
				multi_conv=false;
				showLog=false;
				startConversation();
			}
			else if (inputCode==2){
				System.out.println("Web interface starting...");
				semantics=true;
				multi_conv=true;
				onlineSearching =false;
				showLog=true;
				WebInterface.start();
				}
			else break;
				
		}
		//================================= end		
		//**/


		/**		
		// =============================== testing start			
		initial();
		semantics =true;
		showLog=true;
		Evaluate.testing();
		savingFileName = "testing_JustComment";
	
		Evaluate.evaluating();

		// =============================== testing end	
		**/
	
	


		
	}
	private static void reportState(){
 		
 		String stateString = "";
 		stateString+="Semantics similarity: " + ((semantics==true)?"enabled":"disable");
 		stateString+=", Online query expansion: " + ((onlineSearching==true)?"enabled":"disable");
		stateString+=", Multi-round conversation: " + ((multi_conv==true)?"enabled":"disable");

		System.out.println("System: " + stateString);
 	}
	public static String singleQuey(String query) {
		String inputQuery = query.trim();
		String list [] = threadAgent(inputQuery,  "singleQuey");
		String answer = DataOperation.getCommentFromId(list[0], 0);
		
		System.out.println("Computer: "+ answer);
		return answer;
		
	}
	
	public static void startConversation() {
		
		System.out.println("System: Command line interface start.");
		

		
		reportState();		
		System.out.println("");
		System.out.println("Computer: Hello! I am a bot.");
		
		while (true){
			System.out.print("You: ");
			Scanner scanner = new Scanner(System.in);
			String inputQuery = scanner.nextLine().trim();
			if (inputQuery.trim().equals("[multi=true]")){
				multi_conv=true;
				System.out.println("System: multiRoundConversation = enabled");
				continue;
			}
			if (inputQuery.trim().equals("[multi=false]")){
				multi_conv=false;
				System.out.println("System: multiRoundConversation = disabled");
				continue;
			}
			if (inputQuery.trim().equals("[semantics=false]")){
				semantics=false;
				System.out.println("System: semantics similarity = disabled");
				continue;
			}		
			if (inputQuery.trim().equals("[semantics=true]")){
				semantics=true;
				System.out.println("System: semantics similarity = enabled");
				continue;
			}	
			if (inputQuery.trim().equals("[online=true]")){
				onlineSearching=true;
				System.out.println("System: Online Searching = enabled");
				continue;
			}	
			if (inputQuery.trim().equals("[online=false]")){
				onlineSearching=false;
				System.out.println("System: Online Searching = disabled");
				continue;
			}
			if (inputQuery.trim().equals("[showLog=false]")){
				showLog=false;
				System.out.println("System: Show log = disabled");
				continue;
			}	
			if (inputQuery.trim().equals("[showLog=true]")){
				showLog=true;
				System.out.println("System: Show log = enabled");
				continue;
			}				
			String list [] = threadAgent(inputQuery,  "startConversation");
			System.out.println("Computer: "+ DataOperation.getCommentFromId(list[0], 0));
		}

		
		
	}
		
	
	/**
	 public static String [] method_justCommentForReuse(String inputQuery, String hostCall, String returnOrResult) {
		// if (1==1) return new String[10];
		  savingFileName = "JustCommentWithEmbedingWithOnline";
		  System.out.println("justComment from online: " + inputQuery);
		  
				String[] top10Comment = new String[20];
				Double[] top10CommentScore = new Double[20];	
				
				for (int i = 0; i < top10CommentScore.length; i++) {
		
					top10CommentScore[i] = 0.0;
				}
				
				for (int i = 0; i < top10Comment.length; i++) {
		
					top10Comment[i] = "";
				}
	
			
				Double thresholdForComment = 0.0;		
				
				String queryString = "";
	
				
				if (inputQuery.equals("")){
					Scanner scanner = new Scanner(System.in);
					System.out.println("please enter a new query: ");
					queryString = scanner.nextLine().trim();
				}
				else{
					queryString = inputQuery;
				}
			
				String filteredQuery =  Preprocessing.filterStopWordFromQuery(queryString);
		
				double [] queryWeighting = getQueryWeighting(filteredQuery);
				
				filteredQuery = MyLib_General.deDup(filteredQuery);
				
				
				
				if (MyLib_General.getFieldData(hostCall, 1, "\t").equals("MultiConv")){
					
					String newQueryString = MyLib_General.getFieldData(hostCall, 2, "\t").trim() + " ";
					String queryHistory = MyLib_General.getFieldData(hostCall, 3, "\t").trim()+ " ";
					
					String [] temp = filteredQuery.split(" ");
					
					for (int i =0;i<temp.length;i++){
						if (queryHistory.contains(temp[i]+" ")){
						
							queryWeighting[i]*=0.5;
							System.out.println("penality: " + temp[i]);
						}
					}
					
				}
				
				
				System.out.println("from reuse: \n" + filteredQuery);
				System.out.println();
				
				System.out.println("top 5 responses from computer (rank from high to low): ");
			
				long counter2 = 0;
	
				try {
					BufferedReader reader2 = new BufferedReader(
							new FileReader(path_filter_cmnt_weighted));
	
					String line;
	
					while ((line = reader2.readLine()) != null) {
	
						counter2 += 1;// 5648128 comment
	
						String weighting = MyLib_General.getFieldData(line, 3, "\t");
						
						if (MyLib_General.getFieldData(hostCall, 1, "\t").equals("MultiConv")){
							ArrayList temp = MultiConv.getCommentIdHistory();
							if (temp.indexOf(MyLib_General.getFieldData(line, 1, "\t"))!=-1) continue;
						}
							
						
						
						String [] weightingList = weighting.split(" ");
						double [] weightingarray = new double [weightingList.length];
					
						
						for (int i =0;i<weightingList.length;i++){
							if (weightingList[i].trim().equals("")) continue;
							weightingarray[i] = Double.parseDouble(weightingList[i]); 
						}
						
				double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
		
	
						if (sim_score > thresholdForComment) {
	
							// check duplicated
							boolean duplicated =false;
							
							for (int i=0;i<top10CommentScore.length;i++){
								if (top10Comment[i].equals(line)){
									duplicated = true;
								}
							}
							if (duplicated) continue;
							
							double minimumScore=1;
							int  minimumIndex=0;
							for (int i=0;i<top10CommentScore.length;i++){
								if (minimumScore> top10CommentScore[i]){
									minimumScore = top10CommentScore[i];
									minimumIndex =i;
									
								}
							}
	
							top10CommentScore[minimumIndex] = sim_score;
							top10Comment[minimumIndex] = line;
							
							thresholdForComment= sim_score;
							for (int i=0;i<top10CommentScore.length;i++){
							//	System.out.println(top10PostScore[i]);
								if (thresholdForComment > top10CommentScore[i]){
									thresholdForComment = top10CommentScore[i];
								}
							}
	
						}
					}
					reader2.close();
	
			
					
					
					String [] cmntScoreAndContent = new String[top10CommentScore.length];
					for (int i=0;i<cmntScoreAndContent.length;i++)
						cmntScoreAndContent[i] = top10CommentScore[i]+ "\t"+ top10Comment[i];	
	
					if (semantics){
	
						double v1[] = WordEmbedding.calculateEachSentenceEmbedding(filteredQuery,0);
						
						String resultList [] = new String [cmntScoreAndContent.length];
	
						for (int i=0;i<cmntScoreAndContent.length;i++){
							double [] cmntSentenceEmbedding = new double [200];
							String [] wordVectorString = MyLib_General.getFieldData(cmntScoreAndContent[i], 3, "\t").split(" ");
	
							
							for (int j=0;j<wordVectorString.length;j++){
								double [] wordVector = wordEmbedding.get(wordVectorString[j]);
								if (wordVector ==null) continue;
								for (int k=0;k<wordVector.length;k++) 
									cmntSentenceEmbedding[k] +=wordVector[k];
							}
	
	
							double semantic_score = WordEmbedding.calculateInnerProduct(v1, cmntSentenceEmbedding);
						//	System.out.println(semantic_score+ " "+ cmntScoreAndContent[i])			;	
							double totalScore = semantic_score + Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[i],1, "\t"));
							resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t");
							
						}
						Arrays.sort(resultList,Collections.reverseOrder());
	
						if (returnOrResult=="result") return resultList;
						String [] returnList = new String [11];
						for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(resultList[i], 2, "\t");
					
						returnList[10] = MyLib_General.getFieldData(resultList[0], 1, "\t") + " " +MyLib_General.getFieldData(resultList[9], 1, "\t") ;
						MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(resultList[0], 2, "\t"));					
						
						return returnList;
					}
					else{
						
						Arrays.sort(cmntScoreAndContent,Collections.reverseOrder());
	
						if (returnOrResult=="result") return cmntScoreAndContent;
						
						String returnList [] = new String[11];
						for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(cmntScoreAndContent[i], 2, "\t");
								
						returnList[10] = MyLib_General.getFieldData(cmntScoreAndContent[0], 1, "\t") + " " +MyLib_General.getFieldData(cmntScoreAndContent[9], 1, "\t") ;
						MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(returnList[0], 2, "\t"));					
						
						return returnList;
					}
	
						
				} catch (Exception e) {
					final StringWriter sw = new StringWriter();
					final PrintWriter pw = new PrintWriter(sw, true);
					e.printStackTrace(pw);
					System.out.println(sw.getBuffer().toString());
					sw.getBuffer().toString();
				}
	
				return new String[10];
		}
	**/
	private static double[] getQueryWeighting(String filteredQuery) {
			// TODO Auto-generated method stub
			
			ArrayList vectorString = MyLib_NLP.getVectorString(filteredQuery.trim(), "");
			int[] stringVector = new  int [MyLib_General.deDup(filteredQuery).split(" ").length];
			
			while (filteredQuery.contains("  "))
				filteredQuery = filteredQuery.replace("  ", " ");
	
			String[] temp = filteredQuery.split(" ");
	
	
			for (int i = 0; i < temp.length; i++) {
				stringVector[vectorString.indexOf(temp[i])] += 1;
			}
	
			
			int[] tfUpper = new int [stringVector.length];
			int tfLower = filteredQuery.split(" ").length;
			int commentAmount =0;
			int [] idfLower = new int [stringVector.length];
		
			try {
				commentAmount=  MyLib_General.countLines(BotController.path_filtered_cmnt_weighted);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			ArrayList allTerm = new ArrayList();
			
			BufferedReader reader2;
			try {
				reader2 = new BufferedReader(
						new FileReader(path_term_Frequency));
				
				String line="";
				while ((line = reader2.readLine()) != null) {
					allTerm.add(line);
				}
	
	
				reader2.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			for (int i=0;i<stringVector.length;i++){
		//		System.out.println(vectorString.get(i));
				for (int j=0;j< allTerm.size();j++){
				
					if (MyLib_General.getFieldData(allTerm.get(j).toString(), 1, "\t").equals(vectorString.get(i))){
						idfLower[i] = Integer.parseInt(MyLib_General.getFieldData(allTerm.get(j).toString(), 2, "\t"));
			
						break;
					}else if (j== allTerm.size()-1){
						idfLower[i] =1;//at least get one so that won't infinity
					}
				}
			}
	
			double [] tfidf = new double [stringVector.length];
			
			for (int i =0;i<tfidf.length;i++){
				double idf = Math.log(new Double(commentAmount)/ new Double(idfLower[i]));
				tfidf[i]= (new Double(stringVector[i])/new Double(tfLower)) * idf  ;
				
			}
	
			return tfidf;
		}
	/**
	static void machineLearning(){
		
	//	for (int i=4;i<=10;i++){
			savingFileName = "JustCommentExperiment_" + 2;
			Searching.ML_maxAddCount=2;
			Evaluate.testing();
			Evaluate.evaluating();
		//}
	}
**/
	private static double[] getQueryWeightingFromPost(String filteredQuery) {
		// TODO Auto-generated method stub
		
		ArrayList vectorString = MyLib_NLP.getVectorString(filteredQuery.trim(), "");
		int[] stringVector = new  int [MyLib_General.deDup(filteredQuery).split(" ").length];
	
		while (filteredQuery.contains("  "))
			filteredQuery = filteredQuery.replace("  ", " ");
	
		String[] temp = filteredQuery.split(" ");
	
	
		for (int i = 0; i < temp.length; i++) {
			stringVector[vectorString.indexOf(temp[i])] += 1;
		}
	
		
		int[] tfUpper = new int [stringVector.length];
		int tfLower = filteredQuery.split(" ").length;
		int commentAmount =0;
		int [] idfLower = new int [stringVector.length];
	
		try {
			commentAmount=  MyLib_General.countLines(path_filtered_post_weighted);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		ArrayList allTerm = new ArrayList();
		
		BufferedReader reader2;
		try {
			reader2 = new BufferedReader(
					new FileReader(path_post_term_Frequency));
			
			String line="";
			while ((line = reader2.readLine()) != null) {
				allTerm.add(line);
			}
	
	
			reader2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		for (int i=0;i<stringVector.length;i++){

			for (int j=0;j< allTerm.size();j++){
			
				if (MyLib_General.getFieldData(allTerm.get(j).toString(), 1, "\t").equals(vectorString.get(i))){
					idfLower[i] = Integer.parseInt(MyLib_General.getFieldData(allTerm.get(j).toString(), 2, "\t"));
		
					break;
				}else if (j== allTerm.size()-1){
					idfLower[i] =1;//at least get one so that won't infinity
				}
			}
		}
	
		double [] tfidf = new double [stringVector.length];
		
		for (int i =0;i<tfidf.length;i++){
			double idf = Math.log(new Double(commentAmount)/ new Double(idfLower[i]));
			tfidf[i]= (new Double(stringVector[i])/new Double(tfLower)) * idf  ;
			
		}
	
		return tfidf;
	}
	private static String filterStopWordFromQuery(String query) {
	
		String newQuery = query;
	
		int stopWordCount =0;
		
		try {
			stopWordCount= 1+ MyLib_General.countLines(path_stop_word);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (stopWordCount<=10) System.out.print("problem");
		String[] stopWordList = new String[stopWordCount];
		int tempCount=0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(path_stop_word));
			String line;
			
			while ((line = reader.readLine()) != null) {
		
				stopWordList[tempCount]	= line;
				tempCount+=1;
			}
			reader.close();
	
		} catch (Exception e) {
			System.out.println("cannot find stop word");
		}
		
	
		newQuery = Preprocessing.removePunctuation(newQuery);
	
		newQuery = Preprocessing.removeStopWordFromSentence(" "+ newQuery,stopWordList);
				
	
		return newQuery;
	
	}

	//backup
	/**
	public static String [] method_justComment(String inputQuery) {

		  
		  savingFileName = "JustCommentWithoutEmbedingWithoutOnline";
		 
				String[] top10Comment = new String[20];
				Double[] top10CommentScore = new Double[20];	
				
				for (int i = 0; i < top10CommentScore.length; i++) {
					top10CommentScore[i] = 0.0;
				}
				
				for (int i = 0; i < top10Comment.length; i++) {
					top10Comment[i] = "";
				}

				Double thresholdForComment = 0.0;		
				
				String queryString = "";
				
				
				if (inputQuery.equals("")){
					Scanner scanner = new Scanner(System.in);
					System.out.println("please enter a new query: ");
					queryString = scanner.nextLine().trim();
				}
				else{
					queryString = inputQuery;
				}
			
				String filteredQuery =  Preprocessing.filterStopWordFromQuery(queryString);
		
				double [] queryWeighting = getQueryWeighting(filteredQuery);
				
				
				filteredQuery = MyLib_General.deDup(filteredQuery);
				
				System.out.println("filter query: \n" + filteredQuery);
				System.out.println();
				

				if (multi_conv==true){
					String returnList [] = {MultiConv.caching(filteredQuery)}; 
					if (returnList[0].trim() !="")
						return returnList;
				}
				
				long counter2 = 0;

				try {
					BufferedReader reader2 = new BufferedReader(
							new FileReader(path_filter_cmnt_weighted));

					String line;

					while ((line = reader2.readLine()) != null) {

						counter2 += 1;// 5648128 comment

						String weighting = MyLib_General.getFieldData(line, 3, "\t");
						
						String [] weightingList = weighting.split(" ");
						double [] weightingarray = new double [weightingList.length];
					
						
						for (int i =0;i<weightingList.length;i++){
							if (weightingList[i].trim().equals("")) continue;
							weightingarray[i] = Double.parseDouble(weightingList[i]); 
						}
						
				double sim_score = MyLib_NLP.cosine_Similarity(filteredQuery,queryWeighting ,MyLib_General.deDup(MyLib_General.getFieldData(line, 2, "\t")),weightingarray);
		

						if (sim_score > thresholdForComment) {

							// check duplicated
							boolean duplicated =false;
							
							for (int i=0;i<top10CommentScore.length;i++){
								if (top10Comment[i].equals(line)){
									duplicated = true;
								}
							}
							if (duplicated) continue;
							
							double minimumScore=1;
							int  minimumIndex=0;
							for (int i=0;i<top10CommentScore.length;i++){
								if (minimumScore> top10CommentScore[i]){
									minimumScore = top10CommentScore[i];
									minimumIndex =i;
									
								}
							}

							top10CommentScore[minimumIndex] = sim_score;
							top10Comment[minimumIndex] = line;
							
							thresholdForComment= sim_score;
							for (int i=0;i<top10CommentScore.length;i++){
							//	System.out.println(top10PostScore[i]);
								if (thresholdForComment > top10CommentScore[i]){
									thresholdForComment = top10CommentScore[i];
								}
							}

						}
					}
					reader2.close();

					String [] cmntScoreAndContent = new String[top10CommentScore.length];
					for (int i=0;i<cmntScoreAndContent.length;i++)
						cmntScoreAndContent[i] = top10CommentScore[i]+ "\t"+ top10Comment[i];	
					
					
					
					if (semantics){

						double v1[] = WordEmbedding.calculateEachSentenceEmbedding(filteredQuery,0);
						
						String resultList [] = new String [cmntScoreAndContent.length];

						for (int i=0;i<cmntScoreAndContent.length;i++){
							double [] cmntSentenceEmbedding = new double [200];
							String [] wordVectorString = MyLib_General.getFieldData(cmntScoreAndContent[i], 3, "\t").split(" ");

							
							for (int j=0;j<wordVectorString.length;j++){
								double [] wordVector = wordEmbedding.get(wordVectorString[j]);
								if (wordVector ==null) continue;
								for (int k=0;k<wordVector.length;k++) 
									cmntSentenceEmbedding[k] +=wordVector[k];
							}


							double semantic_score = WordEmbedding.calculateInnerProduct(v1, cmntSentenceEmbedding);
						//	System.out.println(semantic_score+ " "+ cmntScoreAndContent[i])			;	
							double totalScore = semantic_score + Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[i],1, "\t"));
						//	resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t");
							resultList[i] = totalScore + "\t" + MyLib_General.getFieldData(cmntScoreAndContent[i],2, "\t") + "\t" + semantic_score ;
							
						
						}
						Arrays.sort(resultList,Collections.reverseOrder());
					//	System.out.println(Arrays.toString(resultList));
						String [] returnList = new String [11];
						for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(resultList[i], 2, "\t");
					
					//	returnList[10] = MyLib_General.getFieldData(resultList[0], 1, "\t") + " " +MyLib_General.getFieldData(resultList[9], 1, "\t")  ;
						returnList[10] =  MyLib_General.getFieldData(resultList[0], 3, "\t");
						
						System.out.println("topone: " +resultList[9]);
						
						if (Double.parseDouble(MyLib_General.getFieldData(resultList[9], 1, "\t") )<1.0   && onlineSearching == true){
						//System.out.println("dffffffffffffffffffffffff");
							return method_justCommentForReuse(Searching.search(filteredQuery),"justComment","return");
						}
						else{
							
							MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(resultList[0], 2, "\t"));					
							System.out.println(resultList[0]);
							return returnList;
						}
						
					}
					else{
					
						if (Double.parseDouble(MyLib_General.getFieldData(cmntScoreAndContent[9], 1, "\t") )<0.46571182916616827   && onlineSearching == true){
							System.out.println("online");
							return method_justCommentForReuse(Searching.search(filteredQuery),"justComment", "return");
							
						}
						else{
							Arrays.sort(cmntScoreAndContent,Collections.reverseOrder());
						
							String returnList [] = new String[11];
							for (int i=0;i<returnList.length;i++) returnList[i] = MyLib_General.getFieldData(cmntScoreAndContent[i], 2, "\t");
									
							returnList[10] = MyLib_General.getFieldData(cmntScoreAndContent[0], 1, "\t") + " " +MyLib_General.getFieldData(cmntScoreAndContent[9], 1, "\t") ;
							MultiConv.addOneMoreRecord(filteredQuery.replace(" ", ",")+ "\t" + MyLib_General.getFieldData(cmntScoreAndContent[0], 2, "\t"));					
	
							return returnList;
						}
					}		
				} catch (Exception e) {
					final StringWriter sw = new StringWriter();
					final PrintWriter pw = new PrintWriter(sw, true);
					e.printStackTrace(pw);
					System.out.println(sw.getBuffer().toString());
					sw.getBuffer().toString();
				}
				return new String[10];
		}
	**/
}
