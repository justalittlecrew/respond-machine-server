package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import mylibrary.MyLib_General;
import mylibrary.MyLib_NLP;

public class test {
    public static int longestCommonSubsequence(String first, String second) {
        int maxLen = 0;
        int fl = first.split(" ").length;
        int sl = second.split(" ").length;
        String [] firstList = first.split(" ");
        String [] secondList = second.split(" ");
        
        int[][] table = new int[fl+1][sl+1];
     
        for (int i = 1; i <= fl; i++) {
            for (int j = 1; j <= sl; j++) {
                if (firstList[i-1].equals(secondList[j-1])) {
                        table[i][j] = table[i - 1][j - 1] + 1;
                    if (table[i][j] > maxLen)
                        maxLen = table[i][j];
                }
            }
        }
        return maxLen;
    }
    
	static void display(int flag, String id) {

		int counter = 0;
		try {
			String source="";
			if (flag==0) source = "\\repos-id-post-en";
			else source = "\\repos-id-cmnt-en";
			
			BufferedReader reader = new BufferedReader(
				//	new FileReader(System.getProperty("user.dir") + "\\filtered_cmnt"));
				//	new FileReader(System.getProperty("user.dir") + "\\repos-id-cmnt-en"));
			new FileReader(System.getProperty("user.dir") + source));
			//		new FileReader(System.getProperty("user.dir") + "\\post\\filtered_post"));
			String line;
			

			while ((line = reader.readLine()) != null) {
			
				if (line.contains(id)) {
			//	if (counter>1000) break;
					System.out.println(line);
					counter+=1;
			//	return;
				}
			}
		
			reader.close();
		
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		System.out.print("done" + counter);
	}
	public static void test1(){

		Map<String, String> CommentIdAndPostId = new HashMap<>();
		
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\indexFile"));

			String line1;


			while ((line1 = reader.readLine()) != null) {
				String postId = line1.split("\t")[0];
				if (CommentIdAndPostId.containsKey(postId)){
					
				}else CommentIdAndPostId.put(line1.split("\t")[1],postId );	
			}
			reader.close();

		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}

		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "\\train-label"));

			PrintWriter writer = new PrintWriter("train-label-withpostid");
			

			
			String line1;

			while ((line1 = reader.readLine()) != null) {
				//System.out.println(line1);

				writer.println(line1 + "\t" + CommentIdAndPostId.get(MyLib_General.getFieldData(line1, 2, "\t")));

			}
			reader.close();
			writer.close();
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		
	}
}
