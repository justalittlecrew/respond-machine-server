package restful_api;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import core.BotController;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BotController.initial();
		SpringApplication.run(Application.class, args);
		BotController.startConversation();
		
	}

}
