package restful_api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import core.BotController;

@RestController
public class ApplicationRouter {
   @GetMapping("/")
   public String sayHello() {
      return "Hello world! I am a bot.";
   }
   
   @PostMapping(path="/", consumes = "application/json", produces = "application/json")
   public String answer(@RequestBody JSONObject body) {
	  String query = body.getString("query");
	  System.out.println(query);
	  
	  String answerString = BotController.singleQuey(query);
	  
	  JSONObject answer = new JSONObject();
	  answer.put("answer", answerString);
      return answer.toJSONString();
   }   
   
   
   
}