package mylibrary;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

import javax.swing.JComponent;
import javax.swing.JPanel;



public class MyLib_General {

	public static void addItem(JPanel p, JComponent c, int x, int y, int width,
			int height, int align) {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = x;
		gc.gridy = y;
		gc.gridwidth = width;
		gc.gridheight = height;
		gc.weightx = 200.0;
		gc.weighty = 200.0;
		gc.insets = new Insets(5, 5, 15, 5);
		if (align != -1)
			gc.anchor = align;
		gc.fill = GridBagConstraints.NONE;
		p.add(c, gc);
	}

	public static String replaceLast(String string, String from, String to) {

		int lastIndex = string.lastIndexOf(from);
		if (lastIndex < 0)
			return string;
		String tail = string.substring(lastIndex).replace(from, to);
		return string.substring(0, lastIndex) + tail;
	}

	public static String getFieldData(String input, int searchNum,
			String delimiter) {
		try {
			String[] list = input.split(delimiter);
			return list[searchNum - 1];
		} catch (Exception ex) {
		
		}
		return "";

	}

	public static String getDataFromINI(String section, String keyName) {

		boolean start = false;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(
					System.getProperty("user.dir") + "\\config.ini"));
			String line;

			while ((line = reader.readLine()) != null) {
				if (line.startsWith("#"))
					continue;

				if (line.trim().startsWith("[") && line.trim().endsWith("]")) {

					if (("[" + section + "]").equals(line.trim()))
						start = true;

				}

				if (start && line.startsWith(keyName + "=")) {
					reader.close();
					return line.substring(line.indexOf("=") + 1);

				}

			}
			reader.close();

		} catch (Exception e) {

			e.printStackTrace();
			return "[ERROR]";
		}
		return "[ERROR]";
	}

	public static boolean writeDataToINI(String section, String keyName,
			String keyValue) {

		ArrayList settingList = new ArrayList();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(
					System.getProperty("user.dir") + "\\config.ini"));
			String line;
			while ((line = reader.readLine()) != null) {
				if (settingList.size() == 0 && line.trim().length() == 0)
					continue;

				settingList.add(line);
				continue;

			}
			reader.close();

			boolean start = false;
			for (int i = 0; i < settingList.size(); i++) {

				if (settingList.get(i).toString().startsWith("#"))
					continue;

				if (settingList.get(i).toString().trim().startsWith("[")
						&& settingList.get(i).toString().trim().endsWith("]")) {

					if (("[" + section + "]").equals(settingList.get(i)
							.toString().trim()))
						start = true;

				}

				if (start
						&& settingList.get(i).toString()
								.startsWith(keyName + "=")) {
					// return true;
					settingList.add(i, keyName + "=" + keyValue);
					settingList.remove(i + 1);
					break;
				}

				if (i == settingList.size() - 1) { // no this section and
													// keyname, add it
					settingList.add("[" + section + "]");
					settingList.add(keyName + "=" + keyValue);

				}

			}

			try {
				FileWriter fw = new FileWriter(System.getProperty("user.dir")
						+ "\\config.ini");

				for (int i = 0; i < settingList.size(); i++) {
					fw.write(settingList.get(i).toString() + "\r\n");

				//	System.out.print(settingList.get(i).toString() + "\r\n");
				}
				fw.close();
			}

			catch (Exception e) {

				e.printStackTrace();
				return false;
			}

		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}
		return false;

	}

	public static String trimString(String inputString, String targetString) {

		String returnString = inputString;

		if (returnString.trim().length() == 0)
			return returnString;

		if (targetString.trim().length() == 0)
			return "";

		if (returnString.indexOf(targetString) == -1)
			return returnString;

		while (returnString.startsWith(targetString)) {
			returnString = returnString.replaceFirst(targetString, "");
		}
		while (returnString.endsWith(targetString)) {
			returnString = replaceLast(returnString, targetString, "");
		}
		return returnString;

	}

	public static boolean copy(File source, File target)  {
		FileChannel sourceChannel = null;
		FileChannel targetChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			targetChannel = new FileOutputStream(target).getChannel();
			targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
			targetChannel.close();
			sourceChannel.close();
			return true;
		}catch (Exception ex) {

				try {
					targetChannel.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					return false;
				}
				try {
					sourceChannel.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					return false;
				}
				return false;
		}
		finally {
			try {
				targetChannel.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return false;
			}
			try {
				sourceChannel.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
	}
	public static int countLines(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[1024];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } finally {
	        is.close();
	    }
	}

	public static String deDup(String s) {
	    return new LinkedHashSet<String>(Arrays.asList(s.split(" "))).toString().replaceAll("(^\\[|\\]$)", "").replace(", ", " ");
	}
}
