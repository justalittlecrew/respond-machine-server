package online_searching;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import core.BotController;
import core.Preprocessing;
import mylibrary.MyLib_General;
 
public class MaxDuplicateWordCount {
    static String[] stopWordList ; 
    static MaxDuplicateWordCount mdc;
    static Map<String, Integer> wordMap;
    
	public static String removeStopWordFromSentence(String sentence){
		String returnString = sentence.toLowerCase();
				
		for (int i=0;i<stopWordList.length;i++){			
			if (returnString.contains(" " +stopWordList[i].trim()+ " ")){
				System.out.println("gggggg");
				returnString = returnString.replace(" " + stopWordList[i].trim()+ " ", " ");
			}
		}
		return returnString.trim();
	}
	
	static void createStopWord() {


		int stopWordCount =0;
		
		try {
			stopWordCount= 1+ MyLib_General.countLines(BotController.path_stop_word);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (stopWordCount<=10) System.out.print("stop word problem");
		stopWordList = new String[stopWordCount];
		int tempCount=0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(BotController.path_stop_word));
			String line;
			
			while ((line = reader.readLine()) != null) {
		
				stopWordList[tempCount]	= line;
				tempCount+=1;
			}
			reader.close();

		} catch (Exception e) {

		}
		
	
	}
	
	/**
    public Map<String, Integer> getWordCount(String fileName){
 
        FileInputStream fis = null;
        DataInputStream dis = null;
        BufferedReader br = null;
        Map<String, Integer> wordMap = new HashMap<String, Integer>();
        try {
            fis = new FileInputStream(fileName);
            dis = new DataInputStream(fis);
            br = new BufferedReader(new InputStreamReader(dis));
            String line = null;
            while((line = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(line, " ");
                while(st.hasMoreTokens()){
                    String tmp = st.nextToken().toLowerCase();
                    
                    if(wordMap.containsKey(tmp)){
                        wordMap.put(tmp, wordMap.get(tmp)+1);
                    } else {
                        wordMap.put(tmp, 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            try{if(br != null) br.close();}catch(Exception ex){}
        }
        return wordMap;
    }
     **/
    public List<Entry<String, Integer>> sortByValue(Map<String, Integer> wordMap){
         
        Set<Entry<String, Integer>> set = wordMap.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
    }

    public Map<String, Integer> getWordCount(String data, int a){

    	String [] list = data.split(" ");
    
        //Map<String, Integer> wordMap = new HashMap<String, Integer>();
   
        for (int i=0;i<list.length;i++){
        	if (list[i].trim().equals("")) continue;
        	String tmp = list[i].toLowerCase();
        	tmp = Preprocessing.removePunctuation(tmp);
        	if (tmp.trim().equals("")) continue;
        	
        	boolean continueOrNot = true;
        	
        	
        	for (int j=0;j<stopWordList.length;j++) 
        		if (stopWordList[j].equals(tmp)){
        			continueOrNot = false;
        			break;
        		}
        	
        		if (continueOrNot==false) continue;
        	   if(wordMap.containsKey(tmp)){
                  wordMap.put(tmp, wordMap.get(tmp)+1);
               } else {
                   wordMap.put(tmp, 1);
               }
        }

                 
     
        return wordMap;
    }
}