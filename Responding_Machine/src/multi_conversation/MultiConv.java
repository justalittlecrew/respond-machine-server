package multi_conversation;

import java.util.ArrayList;
import java.util.Arrays;

import core.BotController;
import core.DataOperation;
import core.Preprocessing;
import core.Embedding;
import mylibrary.MyLib_General;

public class MultiConv {
	 static ArrayList conversationHistory;   //    keyword1,keyword2,keyword3 \t comment_id
	//  static double stateOneThreshold = 0.0; 
	  
	  static ArrayList commentIdHistory;
	  static int state = 2;     //  1 = different sentence within one post, 2 = expand query
	

	  public static ArrayList getConversationHistory(){
		  return conversationHistory;
	  }
	  public static ArrayList getCommentIdHistory(){
		  return commentIdHistory;
	  }
	  
	  
	private static void retrieveSameCmnt(String newRecord){
			
	}
	  public static void initial(){
			conversationHistory = new ArrayList();
			commentIdHistory  = new ArrayList();	  
	  }
	  
	public static void addOneMoreRecord(String newRecord){
		if (conversationHistory==null) {
			conversationHistory = new ArrayList();
			commentIdHistory  = new ArrayList();
			
			conversationHistory.add(newRecord);
			commentIdHistory.add(MyLib_General.getFieldData(newRecord, 2, "\t"));
		}
		else{
			if (recordExist(newRecord)){
			//	System.out.println("record exist");
			}
			else{
				conversationHistory.add(newRecord);
				commentIdHistory.add(MyLib_General.getFieldData(newRecord, 2, "\t"));
			}
		}
	}
	
	public static void display(){
		System.out.println(Arrays.toString(conversationHistory.toArray()));
	}
	private static boolean recordExist(String newRecord){
		for (int i=0;i<conversationHistory.size();i++){
			if (  MyLib_General.getFieldData(conversationHistory.get(i).toString(), 1, "\t").
					equals(MyLib_General.getFieldData(newRecord,1,"\t"))  ) return true;
		}
		return false;
	}
	public static String caching(String filterQuery){
		
		if (conversationHistory.size()==0) return "";
		
		int queryWordCount = filterQuery.split(" ").length;
		filterQuery = filterQuery.trim()+ " ";
		System.out.println(filterQuery);
		double highestScore =0.0;
		int highestIndex = -1;
		
	
		
		if(state==2){

			String lastKeyword = MyLib_General.getFieldData(conversationHistory.get(conversationHistory.size()-1).toString(),1,"\t").replace(","," ") + " ";
			
			String [] filteredList = filterQuery.split(" ");
			for (int i=0;i<filteredList.length; i++)
				if (lastKeyword.contains(filteredList[i] + " ")) {
					lastKeyword = lastKeyword.replace(filteredList[i]+ " ", " ");
				
				}
		
			
			String newQuery =  filterQuery + lastKeyword.trim();

		//	String list [] = Application.method_justCommentForReuse(newQuery, "MultiConv" + "\t" + filterQuery +"\t" +lastKeyword , "return");
			String list [] = BotController.threadAgent(newQuery, "MultiConv" + "\t" + filterQuery +"\t" +lastKeyword.trim());
		//	System.out.println( DataOperation.getCommentFromId(list[0],0));

			addOneMoreRecord(newQuery.trim().replace(" ", ",") + "\t" +list[0]);
			//addOneMoreRecord(filterQuery + "\t" +list[0]);
			
			return list[0];
		}
		
		
		for (int i=0;i<conversationHistory.size();i++){
			String [] temp = MyLib_General.getFieldData(conversationHistory.get(i).toString(), 1, "\t").split(",");
		
			int matchCount =0;
			for (int j=0;j<temp.length;j++){
			
				if (filterQuery.contains(temp[j] + " ")){
					matchCount +=1;
				
				}
			}
			
			double matchScore = new Double(matchCount)/new Double(queryWordCount);
	
			System.out.println("matchscore:" + matchScore);
			
			
			if (matchScore>highestScore){
				highestScore =matchScore;
				highestIndex = i;
			}
		}
		if (highestIndex==-1) return "";
		
		/**
			if(state==1 && (highestScore > stateOneThreshold)){
			
				String postId = DataOperation.getPostIdFromCmntId(MyLib_General.getFieldData(conversationHistory.get(highestIndex).toString(), 2, "\t"));
				System.out.println("original post: " + DataOperation.getPostFromId(postId));
				
				ArrayList cmntIds = DataOperation.getCommentIdsFromPostId(postId);
				
				ArrayList cmntList = DataOperation.getCmntsFromCmntIds(cmntIds);
				
				double v1[] = WordEmbedding.calculateEachSentenceEmbedding(filterQuery,0);
				double highestCmntScore = 0.0;
				int highestCmntIndex =-1;
				
				for (int i=0;i<cmntList.size();i++){
					
					if (commentIdHistory.indexOf(MyLib_General.getFieldData(cmntList.get(i).toString(), 1, "\t")) != -1) continue;
						System.out.println(cmntList.get(i).toString());
			
					double [] cmntSentenceEmbedding = new double [200];
					//String  eachFormattedCmnt = Preprocessing.removePunctuation(MyLib_General.getFieldData(cmntList.get(i).toString(), 2, "\t")); 
			
					String [] eachFormattedCmntArray = Preprocessing.removePunctuation(MyLib_General.getFieldData(cmntList.get(i).toString(), 2, "\t")).split(" "); 

					for (int j=0;j<eachFormattedCmntArray.length;j++){
						double [] wordVector = Application.wordEmbedding.get(eachFormattedCmntArray[j]);
						if (wordVector ==null) continue;
						for (int k=0;k<wordVector.length;k++) 
							cmntSentenceEmbedding[k] +=wordVector[k];
					}
					
					double semantic_score = WordEmbedding.calculateInnerProduct(v1, cmntSentenceEmbedding);
					if (highestCmntScore<semantic_score){
						highestCmntScore = semantic_score;
						highestCmntIndex = i;
					}
					
				}
				
				addOneMoreRecord(filterQuery.trim().replace(" ", ",") + "\t" + MyLib_General.getFieldData(cmntList.get(highestCmntIndex).toString(),1,"\t"));
			//	System.out.println(highestCmntScore + " " + cmntList.get(highestCmntIndex).toString());
				return MyLib_General.getFieldData(cmntList.get(highestCmntIndex).toString(),1,"\t");
			}
			**/
		
			
						
		
		
		return "";
	}
	
} 
