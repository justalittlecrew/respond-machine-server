package evaluation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import core.BotController;
import mylibrary.MyLib_General;

public class Evaluate {
	/**
	public  static void checkImprovement(){
		String [] observeList = {
				"train-post-10020",
				"train-post-10050",
				"train-post-10060",				
				"train-post-10100",
				"train-post-10270", 
				"train-post-10340",
				"train-post-10440", 
				"train-post-10450",
				"train-post-10510", 
				"train-post-10600",
				"train-post-10670",
				"train-post-10710",
				"train-post-10860", 
				"train-post-10870",
				"train-post-10900", 
				"train-post-10960", 
				"train-post-10970",
				"train-post-10990", 
				};
		
		
		String [] result1 = null;
		result1 = new String[100];

		int counter=0;
		BufferedReader reader1;
		try {
			reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\test\\JustComment"));

			String line1;

			while ((line1 = reader1.readLine()) != null) {
				
				String trainPostId = MyLib_General.getFieldData(line1, 1, "\t");
				

						result1[counter]= line1;
					//	System.out.println(result[counter]);
						counter+=1;
		
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String [] result2 = null;
		result2 = new String[100];

		int counter2=0;
		BufferedReader reader2;
		try {
			reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\test\\JustCommentWithEmbedingWithAllOnline"));

			String line1;

			while ((line1 = reader1.readLine()) != null) {
				
				String trainPostId = MyLib_General.getFieldData(line1, 1, "\t");
				

						result2[counter2]= line1;
					//	System.out.println(result[counter]);
						counter2+=1;
		
				
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		String [] result = null;
		result = new String[100];
		String [] originalResult = new String[100];

		int counter3=0;
		
		for (int i=0;i<result2.length;i++){
			String [] cmntList1 = MyLib_General.getFieldData(result1[i], 2, "\t").split(" ");
			String [] cmntList2 = MyLib_General.getFieldData(result2[i], 2, "\t").split(" ");
			
			if (!cmntList1[0].equals(cmntList2[0])){
				result[counter3]=result2[i];
				originalResult[counter3]=result1[i];
			//	System.out.println(result2[i]);
				counter3++;
			}
			
		}
	//	System.out.println(counter3);
		
	//	result = originalResult;
		
		for (int i=0;i<result.length;i++){
			if (result[i]==null) continue;
			String [] cmntList2 = MyLib_General.getFieldData(result[i], 2, "\t").split(" ");
			String cmntId = cmntList2[0];
		//	System.out.print(MyLib_General.getFieldData(result[i], 1, "\t") + " " + cmntId + " ");
		//	System.out.println(Application.getCommentFromId(cmntId, 0));
			System.out.println(result[i]);
			
		}
		
		
		
		int l2=0;
		int l1=0;
		int l0=0;
		int cannotFind=0;
		for (int i=0;i<result.length;i++){


			BufferedReader reader3;
			try {
				reader3 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\train-label"));
				String line1;
				while ((line1 = reader3.readLine()) != null) {

					if (
							MyLib_General.getFieldData(line1, 1, "\t").equals(MyLib_General.getFieldData(result[i], 1, "\t")) &&
							//MyLib_General.getFieldData(line1, 2, "\t").equals(MyLib_General.getFieldData(result[i], 2, "\t"))
							MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[0].contains(MyLib_General.getFieldData(line1, 2, "\t"))
							
							
							) 
					{
					//	System.out.println(line1);
						if (line1.contains("L1(+1)")){
					//		System.out.println(result[i]);
							l1+=1;
							break;
						}
						else if (line1.contains("L2(+2)")){
					//		System.out.println(result[i]);
							l2+=1;
							break;
						}
						else if (line1.contains("L0(+0)"))
							l0+=1;
						//	lowScore+=Double.parseDouble(MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[10]);
						//	lowScoreCount+=1;
							break;
					}
				
				}
				if (line1==null){
					cannotFind+=1;
				//	lowScore+=Double.parseDouble(MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[11]);
				//	lowScoreCount+=1;
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		


			
		}
		System.out.println("L2: " +l2);

		System.out.println("L1: " +l1);

		System.out.println("L0: " +l0);
		
		
		
		
	}
	**/
	/**
	public static void convertToText(){

		int counter1=0;
		String[] postList = new String[100];
		BufferedReader reader1;
		try {
			reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\train-id-post-en"));

			String line1;

			while ((line1 = reader1.readLine()) != null) {
				postList[counter1++]= MyLib_General.getFieldData(line1, 2,"\t");
				if (counter1>=100) break;
			}
			reader1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	

		int counter=0;
		
		BufferedReader reader2;

		PrintWriter writer; 
		try {
			reader2 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\test\\" + Application.savingFileName));
			writer = new PrintWriter("test//" + Application.savingFileName + "_text");
			
			String line1;

			while ((line1 = reader2.readLine()) != null) {
				String cmntId = MyLib_General.getFieldData(line1, 2, "\t");

				String writeText = "Post" + (counter+1) + ":\t\t" +  postList[counter] + "\r\n" + "Comment" + (counter+1) + ":\t" + Application.getCommentFromId(cmntId,0);
	
				writer.println(writeText);
				counter+=1;
				if (counter>=100) break;
			}
		   	writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		

	}
	**/

	public static String[] getTestingResult() {
		

		String [] result = null;
		try {
			result = new String[MyLib_General.countLines("resources\\test\\" + BotController.savingFileName)];
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int counter=0;
		BufferedReader reader1;
		try {
			reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\resources\\test\\" + BotController.savingFileName));

			String line1;

			while ((line1 = reader1.readLine()) != null) {
				result[counter]= line1;
				counter+=1;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public static double normalizedGainRankOne(){
		
		double totalScore = 0;
		String [] result = getTestingResult();
		int l2=0;
		int l1=0;
		int l0=0;
		int cannotFind =0;
		double lowScore =0.0;
		double lowScoreCount=0;
		for (int i=0;i<result.length;i++){


			BufferedReader reader1;
			try {
				reader1 = new BufferedReader(new FileReader(BotController.path_test_label));
				String line1;
				while ((line1 = reader1.readLine()) != null) {

					if (
							MyLib_General.getFieldData(line1, 1, "\t").equals(MyLib_General.getFieldData(result[i], 1, "\t")) &&
							//MyLib_General.getFieldData(line1, 2, "\t").equals(MyLib_General.getFieldData(result[i], 2, "\t"))
							MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[0].contains(MyLib_General.getFieldData(line1, 2, "\t"))
							
							
							) 
					{
					//	System.out.println(line1);
						if (line1.contains("L1(+1)")){
							//System.out.println(line1);
							totalScore+= (double)1/3;
							l1+=1;
							break;
						}
						else if (line1.contains("L2(+2)")){
					//		System.out.println(line1);
							totalScore+= (double)1;
							l2+=1;
							break;
						}
						else if (line1.contains("L0(+0)"))
							System.out.println(line1);
							l0+=1;
						//	lowScore+=Double.parseDouble(MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[10]);
						//	lowScoreCount+=1;
							break;
					}
				
				}
				if (line1==null){
					cannotFind+=1;
				//	lowScore+=Double.parseDouble(MyLib_General.getFieldData(result[i], 2, "\t").split(" ")[11]);
				//	lowScoreCount+=1;
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		


			
		}
		System.out.println("L2: " +l2);

		System.out.println("L1: " +l1);

		System.out.println("L0: " +l0);
		
		System.out.println("cannot not Find: " +cannotFind);	

		System.out.println("lowScoreAvarge: " + (lowScore/lowScoreCount));	
		
		System.out.print(totalScore/result.length);
		return 0.0;
	}
/**
	public static double normalizedGainRankTen(){
		
		Map<String, String>  label = new HashMap();
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(Application.path_test_label));
			String line;
			  while ((line = reader.readLine()) != null) {
				  String cmntId = MyLib_General.getFieldData(line, 2, "\t");
				  String postId = MyLib_General.getFieldData(line, 1, "\t");
				  String score = MyLib_General.getFieldData(line, 3, "\t");
				//  cmntAndPostEmbeddingScore.put(postId + "\t"+ cmntId,score );
			//	  if (label.get(postId + " "+ cmntId)!= null) System.out.println("duplicated" +postId + " "+ cmntId );
				  label.put(postId + " "+ cmntId,score );
			  }
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		
		System.out.println("label size:" + label.size());
		
		
		double totalScore = 0;
		String [] result = getTestingResult();
		int l2=0;
		int l1=0;
		int l0=0;
		int cannotFind =0;
		for (int i=0;i<result.length;i++){

			String postId =  MyLib_General.getFieldData(result[i], 1, "\t");
			String [] cmntList =  MyLib_General.getFieldData(result[i], 2, "\t").split(" ");
			
			for (int j=0;j<cmntList.length;j++){
				String findPair=postId + " "+ cmntList[j];
				String findScore = label.get(findPair);
				if (findScore ==null) continue;
				else{
					if (findScore.equals("L1(+1)")){
						totalScore+= (double)1/3;
						l1+=1;
						break;
					}
					else if (findScore.equals("L2(+2)")){
						totalScore+= (double)1;
						l2+=1;
						break;
					}
					else if (findScore.equals("L0(+0)"))
						l0+=1;
						break;
				
				}
			}



			
		}
		System.out.println("L2: " +l2);

		System.out.println("L1: " +l1);

		System.out.println("L0: " +l0);
		
		System.out.println("cannot not Find: " +cannotFind);		
		System.out.print(totalScore/result.length);
		return 0.0;
	}
**/
	public static void evaluating() {
		normalizedGainRankOne();
		//normalizedGainRankTen();
	}

	
	public static void testing(){
			
			long tStart = System.currentTimeMillis();
	
			BufferedReader reader1;
			ArrayList resultList = new ArrayList();
			
			try {
				reader1 = new BufferedReader(new FileReader(BotController.path_test_post));
	
				String line1;
				int counter=0;
				while ((line1 = reader1.readLine()) != null) {
	
					counter+=1;
				//	if (counter<=100) continue;
					
					BotController.trainingPost = MyLib_General.getFieldData(line1,1, "\t");
					

					
					String [] resultStringList = BotController.threadAgent((MyLib_General.getFieldData(line1,2, "\t")), "evaluation");
				
					String aggregrateString ="";
					for (int i=0;i<resultStringList.length;i++) aggregrateString+=resultStringList[i]+" ";
					aggregrateString=aggregrateString.trim();
					//System.out.println(line1);
					
					resultList.add(MyLib_General.getFieldData(line1,1, "\t") + "\t" + aggregrateString);
	
					if (counter==100) break;
				}	
				reader1.close();	
				
	
				PrintWriter writer = new PrintWriter("resources//test//"+BotController.savingFileName);
			
					for (int i=0;i<resultList.size();i++){
						writer.println(resultList.get(i));
					}			
	
				writer.close();
				
				long tEnd = System.currentTimeMillis();
				long tDelta = tEnd - tStart;
				double elapsedSeconds = tDelta / 1000.0;
				System.out.print("\ntime:" + elapsedSeconds);
				
				
				
			//	String outputPath3 = "experiment\\101_225Query_expansion";
			//	PrintWriter writer3;
			//	try {
			//		writer3 = new PrintWriter(outputPath3);
			//		for (int i=0;i<experiment.size();i++)
			//		writer3.println(experiment.get(i).toString());
			//		writer3.close();
					
			//	} catch (FileNotFoundException e) {
			//		// TODO Auto-generated catch block
			//		e.printStackTrace();
			//	}
			
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
			
				PrintWriter writer;
				try {
					writer = new PrintWriter("resources//test//"+BotController.savingFileName);
					
					for (int i=0;i<resultList.size();i++){
						writer.println(resultList.get(i));
					}			
	
					writer.close();
				
				long tEnd = System.currentTimeMillis();
				long tDelta = tEnd - tStart;
				double elapsedSeconds = tDelta / 1000.0;
				System.out.print("\ntime:" + elapsedSeconds);
				
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				
				}
	
	
				
				
				e.printStackTrace();
				return;
			}
	
	
		}
		
	/**
	
		static void testingWithOnlineSearching(){
			
			long tStart = System.currentTimeMillis();
	
	
			
			BufferedReader reader1;
			ArrayList resultList = new ArrayList();
			
			try {
				reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\train-id-post-en"));
	
				String line1;
				int counter=0;
				while ((line1 = reader1.readLine()) != null) {
	
					String [] resultStringList = justComment(MyLib_General.getFieldData(line1,2, "\t"));
					
					String aggregrateString ="";
					for (int i=0;i<resultStringList.length;i++) aggregrateString+=resultStringList[i]+" ";
					aggregrateString=aggregrateString.trim();
					System.out.println(line1);
					
					resultList.add(MyLib_General.getFieldData(line1,1, "\t") + "\t" + aggregrateString);
	
					counter+=1;
					if (counter==100) break;
				}	
				reader1.close();	
				
	
				PrintWriter writer = new PrintWriter("test//"+savingFileName);
			
				
					for (int i=0;i<resultList.size();i++){
						writer.println(resultList.get(i));
					
					}			
	
				writer.close();
				
				long tEnd = System.currentTimeMillis();
				long tDelta = tEnd - tStart;
				double elapsedSeconds = tDelta / 1000.0;
				System.out.print("\ntime:" + elapsedSeconds);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
				PrintWriter writer;
				try {
					writer = new PrintWriter("test//"+savingFileName);
					
					for (int i=0;i<resultList.size();i++){
						writer.println(resultList.get(i));
					}			
	
				writer.close();
				
				long tEnd = System.currentTimeMillis();
				long tDelta = tEnd - tStart;
				double elapsedSeconds = tDelta / 1000.0;
				System.out.print("\ntime:" + elapsedSeconds);
				
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				
				}
				
			
			
				e.printStackTrace();
				return;
			}
	
	
		}
	
	**/	
	
}
